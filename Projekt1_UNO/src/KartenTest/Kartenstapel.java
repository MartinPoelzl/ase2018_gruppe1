package KartenTest;

import java.util.Collections;
import java.util.LinkedList;

public class Kartenstapel
{
	private LinkedList<Karten> stapel;

	public Kartenstapel()
	{
		this.stapel = new LinkedList<>();
	}

	public void bef�llen()
	{
		for (int index = 0; index < 2; index++)
		{
			stapel.add(new Plus4());
			stapel.add(new Plus4());
			stapel.add(new Farbwechsel());
			stapel.add(new Farbwechsel());
			stapel.add(new Plus2(Farbe.blau));
			stapel.add(new Plus2(Farbe.gr�n));
			stapel.add(new Plus2(Farbe.gelb));
			stapel.add(new Plus2(Farbe.rot));
			stapel.add(new Aussetzen(Farbe.blau));
			stapel.add(new Aussetzen(Farbe.gr�n));
			stapel.add(new Aussetzen(Farbe.gelb));
			stapel.add(new Aussetzen(Farbe.rot));
			stapel.add(new Richtungswechsel(Farbe.blau));
			stapel.add(new Richtungswechsel(Farbe.gr�n));
			stapel.add(new Richtungswechsel(Farbe.gelb));
			stapel.add(new Richtungswechsel(Farbe.rot));

			for (Integer zahlenwert = 1; zahlenwert < 10; zahlenwert++)
			{
				stapel.add(new Zahlenkarte(Farbe.blau, zahlenwert.toString()));
				stapel.add(new Zahlenkarte(Farbe.gr�n, zahlenwert.toString()));
				stapel.add(new Zahlenkarte(Farbe.gelb, zahlenwert.toString()));
				stapel.add(new Zahlenkarte(Farbe.rot, zahlenwert.toString()));
			}
		}
		stapel.add(new Zahlenkarte(Farbe.blau, "0"));
		stapel.add(new Zahlenkarte(Farbe.gr�n, "0"));
		stapel.add(new Zahlenkarte(Farbe.gelb, "0"));
		stapel.add(new Zahlenkarte(Farbe.rot, "0"));
	}

	public Karten obersteKarte()
	{
		return stapel.getFirst();
	}

	public Karten karteAbheben()
	{
		return stapel.remove(0);
	}
	
	public void addKarten(Karten k)
	{
		stapel.add(k);
	}

	public void mischen()
	{
		Collections.shuffle(stapel);
	}

	public void zeigeStapel()
	{
		System.out.println("Kartenstapel");
		System.out.println("xxxxxxxxxx  ");
		System.out.println("x        x  ");
		if (stapel.size() >= 100)
		{
			System.out.println("x   " + stapel.size() + "  x  ");
		}
		else if (stapel.size() < 10)
		{
			System.out.println("x   " + stapel.size() + "    x  ");
		}
		else
		{
			System.out.println("x   " + stapel.size() + "   x  ");

		}

		System.out.println("x Karten x  ");
		System.out.println("x        x  ");
		System.out.println("xxxxxxxxxx  ");
		System.out.println();
		System.out.println();
	}
	
	public int getSize()
	{
		return stapel.size();
	}

	public void clear()
	{
		stapel.clear();
	}
}
