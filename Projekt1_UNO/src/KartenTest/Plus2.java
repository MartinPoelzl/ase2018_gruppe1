package KartenTest;

public class Plus2 extends ActionKarte
{

	private int punkte;
	
	public Plus2(Farbe farbe)
	{
		super(farbe);
		this.wert = "+2";
		this.punkte=getPunkte();
	}
	
	public int getPunkte()
	{
		punkte = 20;
		return punkte;
	}

}
