package KartenTest;

import java.io.Serializable;

public class Spieler implements Serializable
{
	private static final long serialVersionUID = 2L;
	private String name;
	public Handkarten karten;
	private SpielerEingabe eingabe;

	public Spieler()
	{
		this.karten = new Handkarten();
	}

	public void aufnehmen(Karten karte)
	{
		karten.addKarten(karte);
	}

	public String getName()
	{
		return name;
	}
	
	public void setKarten(Handkarten karten)
	{
		this.karten = karten;
	}

	public Handkarten getKarten()
	{
		return karten;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void zeigeKarten()
	{
		System.out.println("Karten von Spieler: " + getName());
		karten.zeigKarte();
	}

	public String toString()
	{
		return name;
	}

	public Karten ablegen(String se)
	{
		return karten.removeKarte(Integer.parseInt(se));
	}

	public String ablegenOderHeben(String eingabe)
	{	
		String ablegenSE = eingabe;
		
		String x = "";
		
			if (ablegenSE == "a")
			{
				System.out.println("Ihr Spielzug ist beendet");
				x = "a";
				return x;
			} else if (ablegenSE == "h")
			{
				x = "h";
				return x;
			}
		return x;
	}
	
	public int getHandkartenSize()
	{
		return karten.getSize() -1;
	}
	
	public int getAktuelleKartenanzahl()
	{
		return karten.getSize();
	}

}
