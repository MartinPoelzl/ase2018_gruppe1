package KartenTest;

public class Parser
{
	public static Integer tryParseInt(String x)
	{
		try
		{
			return Integer.parseInt(x);
			
		} catch (NumberFormatException e)
		{
			return null;
		}
	}
}
