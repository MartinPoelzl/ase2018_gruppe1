package KartenTest;
import java.util.Iterator;



public class HandkartenIterator implements Iterator<Karten>{

	
	private Handkarten karten;
	private int index;
	
	public HandkartenIterator(Handkarten karten)
	{
		this.karten = karten;
		this.index=0;
	}
	
	@Override
	public boolean hasNext() {
		if (index < karten.size())
			return true;
		return false;
	}

	@Override
	public Karten next() {
		Karten value = karten.get(index);
		index++;
		return value;
	}

}
