package KartenTest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Handkarten extends Karten implements Iterable<Karten>, Serializable 
{
	
	private static final long serialVersionUID = 9L;
	public ArrayList<Karten> hk;


	
	public Handkarten()
	{
		super();
		this.hk = new ArrayList<>();
	}
	
	
	public ArrayList<Karten> getHk()
	{
		return hk;
	}

	public void zeigKarte()
	{
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print(" Karte " + i + "    ");
		}
		System.out.print("\n");
		
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print("xxxxxxxxxx  ");
		}
		System.out.print("\n");
		
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print("x        x  ");
		}
		System.out.print("\n");
		
		for(Karten karte : hk)
		{
			if(karte.getFarbe() == farbe.rot)
			{
				System.out.print("x  " + karte.getFarbe() + "   x  ");
			}
			else if(karte.getFarbe() == farbe.hebe)
			{
				System.out.print("x  " + karte.getFarbe() + "  x  ");
			}
			else if(karte.getFarbe() == farbe.Farben)
			{
				System.out.print("x " + karte.getFarbe() + " x  ");
			}
			else
			{
			System.out.print("x  " + karte.getFarbe() + "  x  ");
			}
		}
		System.out.print("\n");
		
		for(Karten karte : hk)
		{
			if(karte instanceof Plus2 || karte instanceof Plus4)
			{
				System.out.print("x   " + karte.getWert() + "   x  ");
			}
			else if(karte instanceof Richtungswechsel)
			{
				System.out.print("x   " + karte.getWert() + "  x  ");
			}
			else if(karte instanceof Aussetzen)
			{
				System.out.print("x  " + karte.getWert() + " x  ");
			}
			else if(karte instanceof Farbwechsel)
			{
				System.out.print("x " + karte.getWert() + "x  ");
			}
			else
			{
			System.out.print("x   " + karte.getWert() + "    x  ");
			}
		}
		System.out.print("\n");
		
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print("x        x  ");
		}
		System.out.print("\n");
		
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print("xxxxxxxxxx  ");
		}
		System.out.print("\n");
		System.out.println("\n");
	}

	public void addKarten(Karten karte)
	{
		hk.add(karte);
	}

	public String getWert()
	{	
		return wert;
	}

	public Farbe getFarbe()
	{
		return farbe;
	}
	
	public Karten removeKarte(int index)
	{
		return hk.remove(index);
	}
	
	public Karten removeBotKarte(Karten k)
	{
		Karten x;
		int index = 0;
		
		for(int i = 0; i< hk.size(); i++)
		{
			x = hk.get(i);
			if(x.equals(k))
			{
				index = i;
			}
		}
		return hk.remove(index);
	}
	
	
	public int getSize()
	{
		return hk.size();
	}
	


	@Override
	public Iterator<Karten> iterator() {
		HandkartenIterator iterator = new HandkartenIterator(this);
		return iterator;
	}

	public Karten get(int index)
	{
		return hk.get(index);
	}

	public int size() {
		return hk.size();
	}

}