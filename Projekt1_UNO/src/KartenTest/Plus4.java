package KartenTest;

public class Plus4 extends Farbenkarte
{
	private int punkte;
	
	public Plus4()
	{
		this.farbe = Farbe.hebe;
		this.wert = "+4";
		this.punkte=getPunkte();
	}
	
	public int getPunkte()
	{
		punkte = 50;
		return punkte;
	}
	public Farbe farbeWaehlen(String farbe)
	{
		Farbe x = null;
		
		switch (farbe)
		{
		case "r":
			x = Farbe.rot;
			break;
		case "g":
			x = Farbe.gr�n;
			break;
		case "y":
			x = Farbe.gelb;
			break;
		case "b":
			x =  Farbe.blau;
			break;
		}
		return x;
		
	}
}

