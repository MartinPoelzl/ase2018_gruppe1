package KartenTest;

public class Richtungswechsel extends ActionKarte
{
	private int punkte;
	
	public Richtungswechsel(Farbe farbe)
	{
		super(farbe);
		this.wert = "<->";
		this.punkte=getPunkte();
	}
	
	public int getPunkte()
	{
		punkte = 20;
		return punkte;
	}
	
	

}
