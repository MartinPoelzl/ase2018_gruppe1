package KartenTest;

public class Zahlenkarte extends Karten
{
	private int punkte;
	
	public Zahlenkarte(Farbe farbe, String wert)
	{
		super(farbe, wert);
		
		this.punkte=getPunkte();
		
	}

	public String getWert()
	{
		return wert;
	}

	public Farbe getFarbe()
	{
		return farbe;
	}
	
	public  int getPunkte()
	{
		switch(wert)
		{
		case "0":
			punkte = 0;
			break;
		case "1":
			punkte = 1;
			break;
		case "2":
			punkte = 2;
			break;
		case "3":
			punkte = 3;
			break;
		case "4":
			punkte = 4;
			break;
		case "5":
			punkte = 5;
			break;
		case "6":
			punkte = 6;
			break;
		case "7":
			punkte = 7;
			break;
		case "8":
			punkte = 8;
			break;
		case "9":
			punkte = 9;
			break;
		}
		return punkte;
	}

	@Override
	public String toString()
	{
		return "Zahlenkarte [punkte=" + punkte + "]";
	}



	
	

}
