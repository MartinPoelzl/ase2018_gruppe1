package KartenTest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;

public class Bot extends Spieler implements Serializable
{
	private static final long serialVersionUID = 8L;
	private String name;


	public Bot()
	{
		super();
	}

	public boolean vergleiche(Karten as)
	{
	
		for (Karten botKarte : super.getKarten())
		{
			if (as.getFarbe().equals(botKarte.getFarbe()) || as.getWert().equals(botKarte.getWert()) || botKarte instanceof Farbenkarte)
			{
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void aufnehmen(Karten karte)
	{
		super.aufnehmen(karte);
	}

	public Karten botLegtKarte(Karten as)
	{
		if(super.getAktuelleKartenanzahl() == 2)
		{
			System.out.println(name + " sagt 'UNO' \n");
		}
		
		Karten ausgewaehlte = null;

		for (Karten botKarte : super.getKarten())
		{
			if (as.getFarbe().equals(botKarte.getFarbe()) || as.getWert().equals(botKarte.getWert()) || botKarte instanceof Farbenkarte)
			{
				ausgewaehlte = botKarte;
				super.getKarten().removeBotKarte(botKarte);
				break;
			}
		}	
		if(ausgewaehlte instanceof Farbenkarte)
		{
			Farbe f = null;
			int rot = 0;
			int blau = 0;
			int gruen = 0;
			int gelb = 0;
			
			for (Karten farbe : super.getKarten())
			{
				f = farbe.getFarbe();
				switch (f)
				{
				case blau:
					blau++;
					break;
				case rot:
					rot++;
					break;
				case gr�n:
					gruen++;
					break;
				case gelb:
					gelb++;
					break;

				default:
					blau++;
				}
			}
			
			HashMap<Farbe, Integer> liste = new HashMap<>();
			
			liste.put(Farbe.rot, rot);
			liste.put(Farbe.blau, blau);
			liste.put(Farbe.gr�n, gruen);
			liste.put(Farbe.gelb, gelb);
			
			int max = 0;
			Farbe x = null;
			
			for(Entry<Farbe, Integer> farbe : liste.entrySet())
			{
				if(farbe.getValue() > max)
				{
					max = farbe.getValue();
					x = farbe.getKey();
				}
			}
			
			ausgewaehlte.setFarbe(x);
		}
		
		return ausgewaehlte;
	}
	
	public void botHebtKarte(Karten abhebestapel)
	{
		super.getKarten().addKarten(abhebestapel);
	}

	public Handkarten getKarten()
	{
		return super.getKarten();
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void botZeigtKarten()
	{
		System.out.println("Karten von Spieler: " + name);
		super.getKarten().zeigKarte();
	}
	
	
	
	

	
}
