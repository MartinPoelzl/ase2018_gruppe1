package KartenTest;

public class Aussetzen extends ActionKarte
{
	private int punkte;
	
	public Aussetzen(Farbe farbe)
	{
		super(farbe);
		this.wert = "STOPP";
	}
	
	public int getPunkte()
	{
		punkte = 20;
		return punkte;
	}
}
