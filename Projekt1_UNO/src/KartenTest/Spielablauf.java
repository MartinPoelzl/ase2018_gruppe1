package KartenTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import org.campus02.datenbank.DBAbfragen;

public class Spielablauf
{

	private AblegeStapel as;
	private Kartenstapel ks;
	public ArrayList<Spieler> mitspieler = new ArrayList<>();
	private SpielerEingabe se;
	public Spieler aktuellerSpieler;
	private ArrayList<String> namen = new ArrayList<>();
	private DBAbfragen db = new DBAbfragen();
	private int counter;
	private boolean counterControll;
	private boolean uno;
	int spieleranzahl;

	public Spielablauf(AblegeStapel as, Kartenstapel ks, ArrayList<Spieler> mitspieler)
	{
		this.as = as;
		this.ks = ks;
		this.mitspieler = mitspieler;
		this.se = new SpielerEingabe();
		
	}
	
	public Spielablauf()
	{
		
	}

	public Kartenstapel getKs()
	{
		return ks;
	}

	public void spielerAnzahl()
	{
		System.out.println("Wie viele Spieler m�chten mitspielen? 1 - 4 Spieler m�glich");

		String test = se.eingabe();
		spieleranzahl = Parser.tryParseInt(test);

		while (spieleranzahl == 0 || spieleranzahl < 1 || spieleranzahl > 4)
		{			System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
			test = se.eingabe();
			spieleranzahl = Parser.tryParseInt(test);
		}
		spielerName();

	}

	public void zeigeKartenstapel()
	{
		ks.zeigeStapel();
	}

	public Karten abheben()
	{
		Karten temp = null;

		try
		{
			temp = ks.karteAbheben();
		} catch (IndexOutOfBoundsException e)
		{
			while (as.getSize() > 0)
			{
				ks.addKarten(as.remove());
			}

			ks.mischen();
			temp = ks.karteAbheben();

		}
		return temp;
	}

	public void zeigeAblagestapel()
	{
		as.zeigeAblagestapel();
	}

	public void austeilen()
	{
		for (int index = 0; index < 7; index++)
		{
			for (Spieler sp : mitspieler)
			{
				sp.aufnehmen(abheben());
			}
		}
		Karten temp = abheben();
		as.addKarten(temp);

		while (temp instanceof Aktionskarten)
		{
			temp = abheben();
			as.addKarten(temp);
		}
		Collections.shuffle(mitspieler);
	}

	public AblegeStapel KarteAblegen(Karten k)
	{

		if (vergleicheKarten(k) == false)
		{
			System.out.println("Sie k�nnen diese Karte nicht ablegen! \n");

			if (!evaluateCounter())
			{
				return as;
			}

			zeigeSpielerkarten();
			System.out.println(
					"W�hlen Sie eine neue Karte aus Ihrem Deck: Zahl von 0 - " + aktuellerSpieler.getHandkartenSize());

			String test3 = se.eingabe();
			Integer x = Parser.tryParseInt(test3);

			while (x == null || x < 0 || x > aktuellerSpieler.getHandkartenSize())
			{
				if (!evaluateCounter())
				{
					return as;
				}

				System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
				test3 = se.eingabe();
				x = Parser.tryParseInt(test3);
			}
			as = KarteAblegen(aktuellerSpieler.ablegen(test3));

		} else if (vergleicheKarten(k) == true)
		{
			as.addKarten(k);
		}
		return as;
	}

	public void spielerName()
	{
		while (mitspieler.size() != 4)
		{
			int botzahl = 0;
			while (spieleranzahl != 4)
			{
				botzahl++;
				Bot b = new Bot();
				b.setName("Bot " + botzahl);
				mitspieler.add(b);
				spieleranzahl++;
			}

			System.out.println("Geben Sie bitte Ihren Namen ein: ");
			//db.init();
			Spieler x = new Spieler();
			x.setName(se.eingabe());
			if (namen.contains(x.getName()))
			{
				System.out.println("Nickname bereits vergeben, bitte w�hlen Sie einen anderen Namen");
				x.setName(se.eingabe());
			}
			mitspieler.add(x);
			namen.add(x.getName());

			System.out.println("Hallo " + x.getName() + "! Willkommen zu UNO" + "\n");
		}

		for (Spieler spieler : mitspieler)
		{
			db.logIntoSession(spieler.getName());

		}

	}

	public void zeigeSpielerkarten()
	{
		aktuellerSpieler.zeigeKarten();
	}

	public int getSpielerIndex(Spieler x)
	{
		Iterator<Spieler> i = mitspieler.iterator();
		int zaehler = 0;

		while (i.hasNext() && i != x)
		{
			i.next();
			zaehler++;
		}

		return zaehler;
	}

	public boolean vergleicheKarten(Karten k)
	{
		Karten stapelKarte = as.getKartenart();

		if (k instanceof Zahlenkarte || k instanceof ActionKarte)
		{
			if (stapelKarte.getFarbe().equals(k.getFarbe()) || stapelKarte.getWert().equals(k.getWert()))
			{
				return true;
			} else if (stapelKarte.getWert() == k.getWert())
			{
				return true;
			}

		} else if (k instanceof Farbenkarte)
		{
			return true;
		}

		counterControll = false;
		aktuellerSpieler.aufnehmen(k);
		return false;
	}

	public void spielzug()
	{
		aktuellerSpieler = naechsterSpieler();

		if (aktuellerSpieler instanceof Bot)
		{
			botSpielzug();			
		}

		uno = false;
		counter = 3;
		counterControll = true;

		System.out.println("Am Zug ist: " + aktuellerSpieler.getName() + "\n");
		System.out.println();

		zeigeAblagestapel();
		aktuellerSpieler.zeigeKarten();

		if (aktuellerSpieler.getAktuelleKartenanzahl() == 1)
		{
			uno = true;
		} else
		{
			System.out.println("M�chten Sie UNO sagen? Dr�cken Sie 'j' oder 'n'");

			String test = se.eingabe();

			while (!test.equals("j") && !test.equals("n"))
			{
				if (!evaluateCounter())
				{
					return;
				}
				System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
				test = se.eingabe();
			}
			if (test.equals("j"))
			{
				if (!spielerSagtUno())
				{
					return;
				}
			}
		}
		System.out.println("K�nnen Sie eine Karte ablegen oder m�chten Sie eine abheben? Dr�cken Sie 'l' oder 'h'");

		String test2 = se.eingabe();

		while (!test2.equals("l") && !test2.equals("h"))
		{
			if (!evaluateCounter())
			{
				return;
			}
			System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
			test2 = se.eingabe();
		}

		if (test2.equals("l"))
		{

			System.out.println(
					"W�hlen Sie eine Karte aus Ihrem Deck: Zahl von 0 - " + aktuellerSpieler.getHandkartenSize());

			String test3 = se.eingabe();
			Integer x = Parser.tryParseInt(test3);

			while (x == null || x < 0 || x > aktuellerSpieler.getHandkartenSize())
			{
				if (!evaluateCounter())
				{
					return;
				}
				System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
				test3 = se.eingabe();
				x = Parser.tryParseInt(test3);

			}

			as = KarteAblegen(aktuellerSpieler.ablegen(test3));
			zeigeAblagestapel();

			spielerLegtKarte();

		}

		else if (test2.equals("h"))
		{
			if (aktuellerSpieler.getAktuelleKartenanzahl() == 1)
			{
				uno = false;
			}

			aktuellerSpieler.aufnehmen(abheben());
			zeigeAblagestapel();
			zeigeSpielerkarten();

			System.out.println("M�chten Sie UNO sagen? Dr�cken Sie 'j' oder 'n'");

			String teste = se.eingabe();

			while (!teste.equals("j") && !teste.equals("n"))
			{
				if (!evaluateCounter())
				{
					return;
				}
				System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
				teste = se.eingabe();
			}
			if (teste.equals("j"))
			{
				if (!spielerSagtUno())
				{
					return;
				}
			}

			System.out.println("K�nnen Sie eine Karte ablegen oder beenden Sie Ihren Zug? Dr�cken Sie 'l' oder 'b'");

			String test5 = se.eingabe();

			while (!test5.equals("l") && !test5.equals("b"))
			{
				if (!evaluateCounter())
				{
					return;
				}
				System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
				test5 = se.eingabe();
			}

			if (test5.equals("l"))
			{
				System.out.println(
						"W�hlen Sie eine Karte aus Ihrem Deck: Zahl von 0 - " + aktuellerSpieler.getHandkartenSize());

				String test3 = se.eingabe();
				Integer x = Parser.tryParseInt(test3);

				while (x == null || x < 0 || x > aktuellerSpieler.getHandkartenSize())
				{
					if (!evaluateCounter())
					{
						return;
					}
					System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
					test3 = se.eingabe();
					x = Parser.tryParseInt(test3);
				}

				as = KarteAblegen(aktuellerSpieler.ablegen(test3));
				zeigeAblagestapel();
				spielerLegtKarte();

			}
			if (test5.equals("b"))
			{
				System.out.println("Ihr Spielzug ist beendet \n");
			}
		}

		if (aktuellerSpieler.getAktuelleKartenanzahl() == 1 || aktuellerSpieler.getAktuelleKartenanzahl() == 0)
		{
			if (uno == false)
			{
				System.out.println("Sie haben vergessen 'UNO' zu sagen. Sie m�ssen 2 Karten heben \n");
				aktuellerSpieler.aufnehmen(abheben());
				aktuellerSpieler.aufnehmen(abheben());
			}
		}

	}

	public boolean hasPlayerWon()
	{
		return aktuellerSpieler.getAktuelleKartenanzahl() == 0;
	}

	public Spieler naechsterSpieler()
	{
		Spieler s = mitspieler.remove(0);
		mitspieler.add(s);
		return s;
	}

	public void sessionEnde()
	{
		System.out.println(
				"Die Session ist beendet - Danke f�r's Mitspielen! Wenn du weiter spielen m�chtest, logge dich erneut ein.");
		mitspieler.clear();
		namen.clear();
		db.clearSessionListe();

	}

	public void spielEnde()
	{
		System.out.println("Das Spiel ist beendet");
		System.out.println();
		System.out.println(aktuellerSpieler.getName() + " hat gewonnen");
		System.out.println();

		for (int i = 0; i < mitspieler.size(); i++)
		{
			mitspieler.get(i).zeigeKarten();
		}

		ArrayList<Karten> angezeigteKarten = new ArrayList<>();

		for (Spieler sp : mitspieler)
		{
			int summe = 0;
			for (Karten k : sp.getKarten().getHk())
			{
				summe += k.getPunkte();
				angezeigteKarten.add(k);
			}

			String nameAusDatenbank = db.getNameAusDatenbank(sp.getName());

			if (nameAusDatenbank.equals(sp.getName()))
			{
				db.updateSession(summe, sp.getName());
			}
		}

		for (Spieler sp : mitspieler)
		{

			sp.getKarten().getHk().clear();

		}

		as.clear();
		ks.clear();

	}

	public boolean getAllPointsFromDB()
	{

		boolean result = false;

		ArrayList<HashMap<String, String>> punkte = db.getAllPunkteForSession();

		for (HashMap<String, String> map : punkte)
		{
			String s = map.get("SessionScore");
			int p = Integer.parseInt(s);

			if (p >= 500)
			{
				System.out.println("500 Punkte wurden erreicht - die Session ist zu Ende.");
				// an alle Spieler schreiben
				result = true;
			}
		}
		return result;
	}

	public void getHistorieforSession()
	{
		System.out.println("Dies ist die Historie der aktuellen Session:");
		System.out.println(db.getHistorieForSessionID());

	}

	public void getHistorieforPlayer()
	{

		ArrayList<HashMap<String, String>> namen = db.getalleNamenAusDatenbank();

		ArrayList<String> spielerName = new ArrayList<>();

		for (HashMap<String, String> map : namen)
		{
			String nameAusDB = map.get("Nickname");

			spielerName.add(nameAusDB);
		}

		System.out.println("F�r welchen Spieler m�chten Sie die Historie einsehen?");

		String name = se.eingabe();

		while (!spielerName.contains(name))
		{

			System.out.println("Name nicht vorhanden - bitte geben Sie erneut den Namen ein:");
			name = se.eingabe();
		}

		System.out.println("Das ist die Historie f�r den Spieler mit dem Namen: " + name);
		System.out.println(db.getHistorieForNickname(name));

	}

	public String spielerIstAmZug()
	{
		String serverAusgabe = "";

		aktuellerSpieler = mitspieler.get(0);

		System.out.println("Am Zug ist: " + aktuellerSpieler.getName() + "\n");
		System.out.println();

		zeigeAblagestapel();
		aktuellerSpieler.zeigeKarten();
		serverAusgabe = "Am Zug ist: " + aktuellerSpieler.getName() + "\n\n";

		return serverAusgabe;
	}

	public String fragenObUno()
	{
		String serverAusgabe = "";

		System.out.println("M�chten Sie UNO sagen? Dr�cken Sie 'j' oder 'n'");

		serverAusgabe = "M�chten Sie UNO sagen? Dr�cken Sie 'j' oder 'n'" + "\n";

		return serverAusgabe;
	}

	public boolean falscheUnoEingabe(String eingabe)
	{
		
		if (!eingabe.equals("j") && !eingabe.equals("n"))
		{
			System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
	
			return false;
		}
		return true;
	}

	public boolean spielerSagtUno()
	{
	
		System.out.println(aktuellerSpieler.getName() + " sagt UNO \n");
		uno = true;

		if (aktuellerSpieler.getAktuelleKartenanzahl() > 2)
		{
			System.out.println("Sie k�nnen nicht UNO sagen, Sie haben " + aktuellerSpieler.getAktuelleKartenanzahl()
					+ " Karten in der Hand");
			System.out.println("Sie m�ssen 2 Strafkarten heben");
			aktuellerSpieler.aufnehmen(abheben());
			aktuellerSpieler.aufnehmen(abheben());
			naechsterSpieler();
			return false;
		}
		return true;
	}

	public String hebenOderLegen()
	{
		String serverAusgabe = "";

		return serverAusgabe;
	}

	public void spielerLegtKarte()
	{
		if (as.getKartenart() instanceof Farbwechsel)
		{
			System.out.println("W�hlen Sie eine Farbe:");
			System.out.println("'r' = rot; 'g' = gr�n; 'y' = gelb; 'b' = blau");

			String test4 = se.eingabe();

			while (!test4.equals("r") && !test4.equals("g") && !test4.equals("y") && !test4.equals("b"))
			{
				System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
				test4 = se.eingabe();
			}
			as.getKartenart().setFarbe(as.farbeWaehlen(test4));
			zeigeAblagestapel();

		} else if (as.getKartenart() instanceof Aussetzen)
		{
			System.out.println(mitspieler.get(0).getName() + " sie m�ssen leider eine Runde aussetzen \n");

			if (aktuellerSpieler.getAktuelleKartenanzahl() == 1 || aktuellerSpieler.getAktuelleKartenanzahl() == 0)
			{
				if (uno == false)
				{
					System.out.println("Sie haben vergessen 'UNO' zu sagen. Sie m�ssen 2 Karten heben");
					aktuellerSpieler.aufnehmen(abheben());
					aktuellerSpieler.aufnehmen(abheben());
				}
			}
			naechsterSpieler();
			return;

		} else if (as.getKartenart() instanceof Richtungswechsel)
		{
			System.out.println(aktuellerSpieler.getName() + " hat einen Richtungswechsel gespielt \n");

			if (aktuellerSpieler.getAktuelleKartenanzahl() == 1 || aktuellerSpieler.getAktuelleKartenanzahl() == 0)
			{
				if (uno == false)
				{
					System.out.println("Sie haben vergessen 'UNO' zu sagen. Sie m�ssen 2 Karten heben");
					aktuellerSpieler.aufnehmen(abheben());
					aktuellerSpieler.aufnehmen(abheben());
				}
			}

			Collections.reverse(mitspieler);
			naechsterSpieler();
			return;

		}

		else if (as.getKartenart() instanceof Plus2)
		{
			System.out.println(mitspieler.get(0).getName() + " - Sie m�ssen 2 Karten abheben");

			mitspieler.get(0).aufnehmen(abheben());
			mitspieler.get(0).aufnehmen(abheben());

			if (aktuellerSpieler.getAktuelleKartenanzahl() == 1 || aktuellerSpieler.getAktuelleKartenanzahl() == 0)
			{
				if (uno == false)
				{
					System.out.println("Sie haben vergessen 'UNO' zu sagen. Sie m�ssen 2 Karten heben \n");
					aktuellerSpieler.aufnehmen(abheben());
					aktuellerSpieler.aufnehmen(abheben());
				}
			}

			naechsterSpieler();
			return;

		} else if (as.getKartenart() instanceof Plus4)
		{
			System.out.println("W�hlen Sie eine Farbe:");
			System.out.println("'r' = rot; 'g' = gr�n; 'y' = gelb; 'b' = blau");

			String test4 = se.eingabe();

			while (!test4.equals("r") && !test4.equals("g") && !test4.equals("y") && !test4.equals("b"))
			{
				System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
				test4 = se.eingabe();
			}

			as.getKartenart().setFarbe(as.farbeWaehlen(test4));
			zeigeAblagestapel();

			System.out.println(mitspieler.get(0).getName() + " - Sie m�ssen 4 Karten abheben");

			mitspieler.get(0).aufnehmen(abheben());
			mitspieler.get(0).aufnehmen(abheben());
			mitspieler.get(0).aufnehmen(abheben());
			mitspieler.get(0).aufnehmen(abheben());

			if (aktuellerSpieler.getAktuelleKartenanzahl() == 1 || aktuellerSpieler.getAktuelleKartenanzahl() == 0)
			{
				if (uno == false)
				{
					System.out.println("Sie haben vergessen 'UNO' zu sagen. Sie m�ssen 2 Karten heben");
					aktuellerSpieler.aufnehmen(abheben());
					aktuellerSpieler.aufnehmen(abheben());
				}
			}

			naechsterSpieler();
			return;
		}
	}

	public void navigation()
	{

		String auswahl = "";

		while (!auswahl.equals("exit"))
		{

			System.out.println("\nWas m�chten Sie tun?");
			System.out.println(
					"\nHistorie f�r einen Spieler: 1\nHistorie f�r die aktuelle Session: 2\nBeenden und weiter spielen: exit");
			auswahl = se.eingabe();

			switch (auswahl)
			{

			case "1":
				getHistorieforPlayer();
				break;
			case "2":
				getHistorieforSession();
				break;
			case "exit":
				return;
			default:
				System.out.println("Eingabe ung�ltig.");
				break;
			}
		}
	}

	public boolean evaluateCounter()
	{
		if (!counterControll)
		{
			counter--;
			counterControll = true;
		} else
		{
			counter--;
		}

		if (counter == 0)
		{
			counterControll = false;
			System.out.println("Sie haben zu oft eine falsche Eingabe get�tigt, Sie m�ssen eine Karte abheben und");
			System.out.println("Ihr Spielzug ist beendet");
			System.out.println();
			aktuellerSpieler.aufnehmen(abheben());

			return false;
		}
		return true;

	}

	public void botSpielzug()
	{
		Bot b = (Bot) aktuellerSpieler;
	
		zeigeAblagestapel();
		b.botZeigtKarten();

		boolean botAntwort = b.vergleiche(as.getKartenart());

		if (botAntwort)
		{
			as = KarteAblegen(b.botLegtKarte(as.getKartenart()));

			if (as.getKartenart() instanceof Aussetzen)
			{
				System.out.println(mitspieler.get(0).getName() + ", Sie m�ssen leider eine Runde aussetzen \n");
				naechsterSpieler();
			}
			if (as.getKartenart() instanceof Richtungswechsel)
			{
				System.out.println(aktuellerSpieler.getName() + " hat einen Richtungswechsel gespielt \n");
				Collections.reverse(mitspieler);
				naechsterSpieler();
			}
			if (as.getKartenart() instanceof Plus2)
			{
				System.out.println(mitspieler.get(0).getName() + ", Sie m�ssen 2 Karten abheben");

				mitspieler.get(0).aufnehmen(abheben());
				mitspieler.get(0).aufnehmen(abheben());
				naechsterSpieler();
			}
			if (as.getKartenart() instanceof Plus4)
			{
				System.out.println(mitspieler.get(0).getName() + ", Sie m�ssen 4 Karten abheben");

				mitspieler.get(0).aufnehmen(abheben());
				mitspieler.get(0).aufnehmen(abheben());
				mitspieler.get(0).aufnehmen(abheben());
				mitspieler.get(0).aufnehmen(abheben());
				naechsterSpieler();
			}

			return;
		} else
		{
			b.botHebtKarte(abheben());
			b.botZeigtKarten();

			botAntwort = b.vergleiche(as.getKartenart());
			if (botAntwort)
			{
				as = KarteAblegen(b.botLegtKarte(as.getKartenart()));

				if (as.getKartenart() instanceof Aussetzen)
				{
					System.out.println(mitspieler.get(0).getName() + ", Sie m�ssen leider eine Runde aussetzen \n");
					naechsterSpieler();
				}
				if (as.getKartenart() instanceof Richtungswechsel)
				{
					System.out.println(aktuellerSpieler.getName() + " hat einen Richtungswechsel gespielt \n");
					Collections.reverse(mitspieler);
					naechsterSpieler();
				}
				if (as.getKartenart() instanceof Plus2)
				{
					System.out.println(mitspieler.get(0).getName() + ", Sie m�ssen 2 Karten abheben");

					mitspieler.get(0).aufnehmen(abheben());
					mitspieler.get(0).aufnehmen(abheben());
					naechsterSpieler();
				}
				if (as.getKartenart() instanceof Plus4)
				{
					System.out.println(mitspieler.get(0).getName() + ", Sie m�ssen 4 Karten abheben");

					mitspieler.get(0).aufnehmen(abheben());
					mitspieler.get(0).aufnehmen(abheben());
					mitspieler.get(0).aufnehmen(abheben());
					mitspieler.get(0).aufnehmen(abheben());
					naechsterSpieler();
				}
				return;
			}
			return;
		}

	}
}
