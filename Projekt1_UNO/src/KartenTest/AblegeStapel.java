package KartenTest;

import java.util.LinkedList;

public class AblegeStapel extends Karten
{
	
	private static final long serialVersionUID = 4L;
	private LinkedList<Karten> stapel;

	public AblegeStapel()
	{
		this.stapel = new LinkedList<>();
	}

	public void zeigeAblagestapel()
	{
		Karten k = stapel.peekLast();
		
		System.out.println("Ablagestapel");
		System.out.println("xxxxxxxxxx");
		System.out.println("x        x");

		if (k.getFarbe() == farbe.rot)
		{
			System.out.print("x  " + k.getFarbe() + "   x  ");
		} else if (k.getFarbe() == farbe.hebe)
		{
			System.out.print("x  " + k.getFarbe() + "  x  ");
		} else if (k.getFarbe() == farbe.Farben)
		{
			System.out.print("x " + k.getFarbe() + " x  ");
		} else
		{
			System.out.print("x  " + k.getFarbe() + "  x  ");
		}

		System.out.print("\n");

			if (k instanceof Plus2 || k instanceof Plus4)
			{
				System.out.print("x   " + k.getWert() + "   x  ");
			} else if (k instanceof Richtungswechsel)
			{
				System.out.print("x   " + k.getWert() + "  x  ");
			} else if (k instanceof Aussetzen)
			{
				System.out.print("x  " + k.getWert() + " x  ");
			} else if (k instanceof Farbwechsel)
			{
				System.out.print("x " + k.getWert() + "x  ");
			} else
			{
				System.out.print("x   " + k.getWert() + "    x  ");
			}
		
		System.out.print("\n");
		System.out.print("x        x  ");
		System.out.print("\n");
		System.out.print("xxxxxxxxxx  ");
		System.out.print("\n");
		System.out.println();
		System.out.println();
		
	}

	@Override
	public String getWert()
	{
		return wert;
	}

	@Override
	public Farbe getFarbe()
	{
		return farbe;
	}
	
	public int getSize()
	{
		return stapel.size();
	}
	
	public Karten remove()
	{
		return stapel.removeFirst();
	}
	
	public void addKarten(Karten k)
	{
		stapel.addLast(k);
	}
	
	public Karten getKartenart()
	{
		return stapel.peekLast();
	}
	
	public void clear()
	{
		stapel.clear();
	}

}
