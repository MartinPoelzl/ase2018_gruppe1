package KartenTest;

public class Aktionskarten extends Karten
{
	private int punkte;

	public Aktionskarten(Farbe farbe, String wert)
	{
		super(farbe, wert);
		
		this.punkte=getPunkte();
	}

	public Aktionskarten(Farbe farbe)
	{
		super(farbe);
		this.punkte=getPunkte();
	}
	
	public Aktionskarten()
	{
		this.punkte=getPunkte();
	}

	public String getWert()
	{
		return wert;
	}

	public Farbe getFarbe()
	{
		return farbe;
	}
	

	

	@Override
	public String toString()
	{
		return "Aktionskarten [punkte=" + punkte + "]";
	}

	

	
	
}
