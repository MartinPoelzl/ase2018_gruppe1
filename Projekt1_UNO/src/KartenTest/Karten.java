package KartenTest;

import java.io.Serializable;

public class Karten implements Serializable
{
	private static final long serialVersionUID = 3L;
	public Farbe farbe;
	public String wert;
	private int punkte;
	
	public Karten(Farbe farbe, String wert)
	{
		this.farbe = farbe;
		this.wert = wert;
	}
	
	public Karten(String wert)
	{

		this.wert = wert;
	}
	
	public Karten()
	{
		
	}
	public Karten(Farbe farbe)
	{
		this.farbe = farbe;
	}
	
	public int getPunkte() {
		return punkte;
	}
	
	
	public String getWert()
	{
		return wert;
	}
	
	public Farbe getFarbe()
	{
		return farbe;
	}
	
	public void setFarbe(Farbe farbe)
	{
		this.farbe = farbe;
	}
	public void setWert(String wert)
	{
		this.wert = wert;
	}
	public Farbe farbeWaehlen(String farbe)
	{
		Farbe x = null;
		
		switch (farbe)
		{
		case "r":
			x = Farbe.rot;
			break;
		case "g":
			x = Farbe.gr�n;
			break;
		case "y":
			x = Farbe.gelb;
			break;
		case "b":
			x =  Farbe.blau;
			break;
		}
		return x;
	}

	
	
}
