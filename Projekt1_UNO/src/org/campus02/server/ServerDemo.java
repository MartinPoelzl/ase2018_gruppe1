package org.campus02.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;

import org.campus02.datenbank.DBAbfragen;

import KartenTest.Bot;

public class ServerDemo
{

	private static float time;

	public static void main(String[] args) throws IOException, ClassNotFoundException
	{
		// starteServer();

		DBAbfragen db = null;
		GameManager gm = null;

		try
		{
			ServerSocket server = new ServerSocket(1111);

			ArrayList<Socket> players = new ArrayList<>();
			ArrayList<Thread> loginThreads = new ArrayList<>();
			ArrayList<Thread> naviThreads = new ArrayList<>();
			Socket socket = null;
			ObjectInputStream ois = null;
			ObjectOutputStream oos = null;
			PlayerServer ps = null;

			db = new DBAbfragen();
			gm = new GameManager(db);

			while (true)
			{
				server.setSoTimeout(1000);

				Date start = null;

				while (gm.namen.size() != 4 && time <= 10000)
				{
					try
					{
						socket = server.accept();
						System.out.println("Accepted: " + socket);

						ps = new PlayerServer(socket);
						gm.setPs(ps);

						if (players.size() == 0)
						{
							start = new Date();
						}
						players.add(socket);

						ois = new ObjectInputStream(socket.getInputStream());
						oos = new ObjectOutputStream(socket.getOutputStream());
						ps.setOis(ois);
						ps.setOos(oos);

						Thread t1 = new Thread(new RegisterRunnable(socket, gm, db, ois, oos, ps));
						loginThreads.add(t1);
						t1.start();
										

					} catch (SocketTimeoutException so)
					{

						if (start != null)
						{
							Date current = new Date();
							time = current.getTime() - start.getTime();

							if (time > 10000)
							{
								int diff = 4 - players.size();
								int botzahl = 0;
								for (int i = 0; i < diff; i++)
								{
									botzahl++;
									Bot b = new Bot();
									b.setName("Bot " + botzahl);
									gm.mitspieler.add(b);
									db.logIntoSession(b.getName());

									System.out.println(b.getName() + " wurde hinzugefügt");
								}

							}
						}

					}
				}
				if(players.size() > gm.namen.size())
				for (Thread t : loginThreads)
				{
					t.join();
				}

				while (!gm.getAllPointsFromDB())
				{

					gm.spielStart();

					for (PlayerServer s : gm.psList)
					{

						Thread t2 = new Thread(
								new NavigationRunnable(s.getSocket(), gm, db, s.getOis(), s.getOos(), s));
						naviThreads.add(t2);
						t2.start();
					}

					for (Thread t : naviThreads)
					{
						t.join();
					}

					naviThreads.clear();
				}

				gm.sessionEnde();
				players.clear();
				loginThreads.clear();
				time = 0;

//				Sessionende

			}
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}