package org.campus02.server;

import java.io.Serializable;

import KartenTest.Farbe;

public class FrageAntwort implements Serializable
{

	private String text;
	private Farbe farbe;
	private boolean heben;
	private static final long serialVersionUID = 5L;

	public FrageAntwort(String text)
	{
		this.text = text;
	}
	
	public FrageAntwort(String text, Farbe farbe)
	{
		this.text = text;
		this.farbe = farbe;
	}
	
	public FrageAntwort()
	{
		
	}
	
	public FrageAntwort(String text, Farbe farbe, boolean heben)
	{
		this.text = text;
		this.farbe = farbe;
		this.heben = heben;
	}
	
	
	public boolean getHeben()
	{
		return heben;
	}

	public void setHeben(boolean heben)
	{
		this.heben = heben;
	}

	public Farbe getFarbe()
	{
		return farbe;
	}

	public void setFarbe(Farbe farbe)
	{
		this.farbe = farbe;
	}


	public String getText()
	{
		return text;
	}

	public void setText(String frage)
	{
		this.text = frage;
	}

	
	
}
