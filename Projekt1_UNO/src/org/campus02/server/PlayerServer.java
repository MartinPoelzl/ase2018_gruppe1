package org.campus02.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import KartenTest.Spieler;

public class PlayerServer
{

	private Socket socket;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private String name;
	private Spieler spieler;
	
	
	public PlayerServer(Socket socket)
	{
		this.socket = socket;
	}


	public ObjectInputStream getOis()
	{
		return ois;
	}


	public void setOis(ObjectInputStream ois)
	{
		this.ois = ois;
	}


	public ObjectOutputStream getOos()
	{
		return oos;
	}


	public void setOos(ObjectOutputStream oos)
	{
		this.oos = oos;
	}


	public String getName()
	{
		return name;
	}


	public void setName(String name)
	{
		this.name = name;
	}


	public Socket getSocket()
	{
	
		return socket;
	}


	public void setSocket(Socket socket)
	{
		this.socket = socket;
	}
	
	public Object readObject() throws IOException, ClassNotFoundException
	{
		return ois.readObject();
	}

	public void writeObject(Object o) throws IOException
	{
		oos.writeObject(o);
		oos.flush();
		oos.reset();
	}


	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerServer other = (PlayerServer) obj;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	public Spieler getSpieler()
	{
		return spieler;
	}


	public void setSpieler(Spieler spieler)
	{
		this.spieler = spieler;
	}

	
	
	
}
