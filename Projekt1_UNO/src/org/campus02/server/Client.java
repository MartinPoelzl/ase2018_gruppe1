package org.campus02.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import KartenTest.AblegeStapel;
import KartenTest.ActionKarte;
import KartenTest.Farbe;
import KartenTest.Farbenkarte;
import KartenTest.Karten;
import KartenTest.Kartenstapel;
import KartenTest.Parser;
import KartenTest.Spieler;
import KartenTest.Zahlenkarte;

public class Client
{

	private static String spielername = null;
	public static boolean spieleruno;
	public static boolean uno;
	public static boolean spielEnde;
	public static boolean sessionEnde;

	public static void main(String[] args)
	{
		// "192.168.1.104" Miki zu Hause
		// "172.32.0.120" Lisa
		// "172.32.0.126" Kathi
		// "172.32.1.31" Miki

		try (Socket clientSocket = new Socket("localhost", 1111);
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());)
		{

			System.out.println("Willkommen zu Uno -  powered by Lisa, Kathi, Theresa und Michi");
			System.out.println();
			System.out.println("Bitte geben Sie Ihren Namen ein: ");

			String name = br.readLine();
			spielername = name;

			oos.writeObject(name);
			oos.flush();

			String nameVergeben = "Dieser Name ist bereits vergeben, bitte geben Sie einen neuen Namen ein:";
			String checkName = (String) ois.readObject();

			while (checkName.equals(nameVergeben))
			{
				System.out.println(nameVergeben);
				String newName = br.readLine();

				oos.writeObject(newName);
				oos.flush();

				checkName = (String) ois.readObject();
			}

			System.out.println(checkName);

			System.out.println();
			System.out.println();

			// Spielstart
			// wiederhole das Spiel
			do
			{

				ServerObject so = new ServerObject();
				FrageAntwort fa = new FrageAntwort();
				Kartenstapel ks = new Kartenstapel();
				AblegeStapel as = new AblegeStapel();

				// jeder Spieler bekommt nach dem Austeilen seine Karten
				so = (ServerObject) ois.readObject();
				ks.addKarten(so.getKs());
				as.addKarten(so.getAs());

				System.out.println(so.getTodo() + "\n"); // "Spiel wird gestartet"
				as.zeigeAblagestapel();
				so.getSpieler().zeigeKarten();
				sessionEnde = false;

				spielEnde = false;

				// wiederhole den Spielzug
				while (!spielEnde)
				{

					String x = "";

					// wiederhole, solange der Spieler nicht dran ist
					do
					{
						
						// solange das Spiel nicht beendet ist wiederholt es sich
						if (x.contains("Das Spiel ist beendet! "))
						{
							spielEnde = true;
							break;
						}

						// jeder Spieler erf�hrt wer am Zug ist
						fa = (FrageAntwort) ois.readObject();

						System.out.println(fa.getText() + "\n");
						System.out.println();

						x = fa.getText();

					} while (!x.contains("Am Zug ist: " + spielername));

					if (spielEnde)
					{
						break;
					}

					// Spieler ist am Zug

					uno = false;
					spieleruno = false;
					// counter = 10;

					// wenn Spieler an der Reihe ist, bekommt er aktuellen AS und seine HK
					so = (ServerObject) ois.readObject();

					as.addKarten(so.getAs());
					System.out.println(so.getTodo()); // "Sie sind am Zug"
					System.out.println();
					as.zeigeAblagestapel();
					so.getSpieler().zeigeKarten();

					// es wird kontrolliert, ob der Spieler mehr als eine Karte hat
					if (so.getSpieler().getAktuelleKartenanzahl() == 1)
					{
						uno = true;
					} else
					{

						// Spieler wird gefragt ob er UNO sagen m�chte
						fa = (FrageAntwort) ois.readObject();
						System.out.println(fa.getText());

						// Spieler schickt UNO sagen ja oder nein zur�ck
						String todo = br.readLine();
						fa.setText(todo);
						oos.writeObject(fa);
						oos.flush();
						String clientAntwort = todo;

						// solange falsche Eingabe get�tigt wird
						while (!clientAntwort.equals("j") && !clientAntwort.equals("n"))
						{
							// Falsche Eingabe - bitte wiederholen
							fa = (FrageAntwort) ois.readObject();
							System.out.println(fa.getText());

							// Spieler macht neue Eingabe und schickt sie zur�ck
							String neueAntwort = br.readLine();

							fa.setText(neueAntwort);
							oos.writeObject(fa);
							oos.flush();
							clientAntwort = neueAntwort;
						}

						// Spieler m�chte UNO sagen
						if (clientAntwort.equals("j"))
						{
							uno = true;
							// allen Spielern wird UNO geschickt
							fa = (FrageAntwort) ois.readObject();
							System.out.println(fa.getText());

							// wenn der Spieler mehr als 2 Karten in der Hand hat
							if (so.getSpieler().getAktuelleKartenanzahl() > 2)
							{
								spieleruno = true;
								// bekommt 2 Strafkarten - Spielzugs soll beendet werden und wieder an den
								// Anfang der Schleife
								so = (ServerObject) ois.readObject();
								so.getSpieler().zeigeKarten();
								System.out.println(so.getTodo());
								continue;
							}
						}
					}

					if (!spieleruno)
					{
						// Spieler wird gefragt ob er legen oder heben m�chte
						fa = (FrageAntwort) ois.readObject();
						System.out.println(fa.getText());

						// Spieler schickt legen oder heben zur�ck
						String todo = br.readLine();
						fa.setText(todo);
						oos.writeObject(fa);
						oos.flush();
						String clientAntwort = todo;

						// solange falsche Eingabe get�tigt wird
						while (!clientAntwort.equals("l") && !clientAntwort.equals("h"))
						{
							// Falsche Eingabe - bitte wiederholen
							fa = (FrageAntwort) ois.readObject();
							System.out.println(fa.getText());

							// Spieler macht neue Eingabe und schickt sie zur�ck
							String neueAntwort = br.readLine();

							fa.setText(neueAntwort);
							oos.writeObject(fa);
							oos.flush();
							clientAntwort = neueAntwort;
						}

						// wenn Spieler legen m�chte
						if (clientAntwort.equals("l"))
						{
							SpielerWillLegen(fa, so, ois, oos, br, as);

						}

						// wenn Spieler heben m�chte
						else if (clientAntwort.equals("h"))
						{
							// Spieler bekommt die aktualisierten Handkarten
							so = (ServerObject) ois.readObject();
							so.getSpieler().zeigeKarten();

							// Spieler wird gefragt, ob er UNO sagen m�chte
							fa = (FrageAntwort) ois.readObject();
							System.out.println(fa.getText());

							// Spieler sendet j oder n
							todo = br.readLine();
							fa.setText(todo);

							oos.writeObject(fa);
							oos.flush();
							clientAntwort = todo;

							// solange falsche Eingabe get�tigt wird
							while (!clientAntwort.equals("j") && !clientAntwort.equals("n"))
							{
								// Falsche Eingabe - bitte wiederholen
								fa = (FrageAntwort) ois.readObject();
								System.out.println(fa.getText());

								// Spieler macht neue Eingabe und schickt sie zur�ck
								String neueAntwort = br.readLine();

								fa.setText(neueAntwort);
								oos.writeObject(fa);
								oos.flush();
								clientAntwort = neueAntwort;
							}

							// Spieler m�chte UNO sagen
							if (clientAntwort.equals("j"))
							{
								uno = true;
								// allen Spielern wird UNO geschickt
								fa = (FrageAntwort) ois.readObject();
								System.out.println(fa.getText());

								// wenn der Spieler mehr als 2 Karten in der Hand hat
								if (so.getSpieler().getAktuelleKartenanzahl() > 2)
								{
									spieleruno = true;
									// Spieler bekommt aktualisierte Hand
									so = (ServerObject) ois.readObject();
									so.getSpieler().zeigeKarten();
									System.out.println(so.getTodo());
									// bekommt 2 Strafkarten - Spielzugs soll beendet werden und wieder an den
									// Anfang der Schleife
									continue;
								}
							}
							if (!spieleruno)
							{
								// Spieler wird gefragt ob er legen oder beenden m�chte
								fa = (FrageAntwort) ois.readObject();
								System.out.println(fa.getText());

								// Spieler schickt l oder b zur�ck
								todo = br.readLine();
								fa.setText(todo);

								oos.writeObject(fa);
								oos.flush();
								clientAntwort = todo;

								// solange der Spieler falsche Eingabe t�tigt
								while (!clientAntwort.equals("l") && !clientAntwort.equals("b"))
								{
									// Spieler bekommt Antwort, dass die Eingabe falsch war
									fa = (FrageAntwort) ois.readObject();
									System.out.println(fa.getText());

									// Spieler macht neue Eingabe und schickt sie zur�ck
									String neueAntwort = br.readLine();

									fa.setText(neueAntwort);
									oos.writeObject(fa);
									oos.flush();
									clientAntwort = neueAntwort;
								}

								// wenn Spieler legen m�chte
								if (clientAntwort.equals("l"))
								{
									SpielerWillLegen(fa, so, ois, oos, br, as);
								}

							} else if (clientAntwort.equals("b"))
							{
								// ihr Spielzug ist beendet
								fa = (FrageAntwort) ois.readObject();
							}
						}
					}

				} // aktuelle Spiel ist beendet

				// empfangt Auswahl von Navi-M�glichkeiten

				String naviAuswahl = (String) ois.readObject();
				System.out.println(naviAuswahl);

				String antwortF�rNavi = br.readLine();

				// schickt Navi-Auswahl bis Client "exit" dr�ckt
				do

				{
					oos.writeObject(antwortF�rNavi);
					oos.flush();

					// Wenn Client "1" dr�ckt wird Historie f�r Spieler aufgerufen
					if (antwortF�rNavi.equals("1"))
					{ // NAmen empfangen, richtiges Ergebnis schicken
						System.out.println("F�r welchen Spieler m�chten Sie die Historie einsehen?");
						String diesenNamen = br.readLine();
						oos.writeObject(diesenNamen);

						oos.flush();

						@SuppressWarnings("unchecked")
						ArrayList<HashMap<String, String>> ergebnisF�rNamen = (ArrayList<HashMap<String, String>>) ois
								.readObject();
						if (ergebnisF�rNamen.size() == 0)
						{
							System.out.println("Name nicht vorhanden.");
						} else
						{
							System.out.println(ergebnisF�rNamen);
						}
						naviAuswahl = (String) ois.readObject();
						System.out.println(naviAuswahl);
						antwortF�rNavi = br.readLine();
						continue;

					}

					// wenn Client "2" dr�ckt wird Historie f�r Session abgefragt
					if (antwortF�rNavi.equals("2"))
					{

						System.out.println("Dies ist die Historie der aktuellen Session:");
						@SuppressWarnings("unchecked")
						ArrayList<HashMap<String, String>> ergebnisF�rSession = (ArrayList<HashMap<String, String>>) ois
								.readObject();
						System.out.println(ergebnisF�rSession);
						naviAuswahl = (String) ois.readObject();
						System.out.println(naviAuswahl);
						antwortF�rNavi = br.readLine();
						continue;
					}
					if (antwortF�rNavi.equals("exit"))
					{
						break;
					} else
					{
						System.out.println("Ung�ltige Eingabe - bitte erneut Anfrage eingeben:");
						naviAuswahl = (String) ois.readObject();
						System.out.println(naviAuswahl);
						antwortF�rNavi = br.readLine();
						continue;
					}

				} while (!antwortF�rNavi.equals("exit"));

				oos.writeObject(antwortF�rNavi);
				oos.flush();

				String navi = (String) ois.readObject();
				System.out.println(navi);

				if (navi.equals(
						"Die Session ist beendet - Danke f�r's Mitspielen! Wenn du weiter spielen m�chtest, logge dich erneut ein."))
				{
					sessionEnde = true;
					return;
				}

			} while (!sessionEnde);

		} catch (IOException e)
		{
			e.printStackTrace();

		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

	}

	public static boolean vergleicheKarte(Karten k, Karten as, Spieler s)
	{
		Karten stapelKarte = as;

		if (k instanceof Zahlenkarte || k instanceof ActionKarte)
		{
			if (stapelKarte.getFarbe().equals(k.getFarbe()) || stapelKarte.getWert().equals(k.getWert()))
			{
				return true;
			} else if (stapelKarte.getWert() == k.getWert())
			{
				return true;
			}

		} else if (k instanceof Farbenkarte)
		{
			return true;
		}

		s.zeigeKarten();
		return false;
	}

	public static void aufnehmen(Karten karte, Spieler s)
	{
		s.karten.addKarten(karte);
	}

	public static Farbe farbeWaehlen(String farbe)
	{
		Farbe x = null;

		switch (farbe)
		{
		case "r":
			x = Farbe.rot;
			break;
		case "g":
			x = Farbe.gr�n;
			break;
		case "y":
			x = Farbe.gelb;
			break;
		case "b":
			x = Farbe.blau;
			break;
		}
		return x;
	}

	public static void SpielerWillLegen(FrageAntwort fa, ServerObject so, ObjectInputStream ois, ObjectOutputStream oos,
			BufferedReader br, AblegeStapel as) throws IOException, ClassNotFoundException
	{

		// Spieler bekommt Frage mit welche Karte
		fa = (FrageAntwort) ois.readObject();
		System.out.println(fa.getText());

		// Spieler gibt Zahl ein
		String clientAntwort2 = br.readLine();
		fa.setText(clientAntwort2);

		// �berpr�fung ob es eine Zahl ist - passiert nur am Client
		Integer r = Parser.tryParseInt(clientAntwort2);
		Integer zahl = r;

		// solange nicht eine korrekte Zahl eingegeben wird
		while ((zahl == null || zahl < 0 || zahl > so.getSpieler().getHandkartenSize()) && !clientAntwort2.equals("h"))
		{
			// Falsche Zahl eingegeben
			System.out.println("Falsche Eingabe, bitte wiederholen Sie die Eingabe");

			// Spieler macht neue Eingabe
			String neueAntwort = br.readLine();
			fa.setText(neueAntwort);

			r = Parser.tryParseInt(neueAntwort);
			clientAntwort2 = neueAntwort;
			zahl = r;
		}

		// Spieler m�chte doch heben
		if (clientAntwort2.equals("h"))
		{
			so.getSpieler().aufnehmen(so.getKs());
			so.getSpieler().zeigeKarten();
			fa.setHeben(true);
			oos.writeObject(fa);
			oos.flush();
			System.out.println("Ihr Spielzug ist beendet");
			return;
		}

		Farbe farbe = null;
		String test4 = "";

		// Kontrolle, ob Karte auf den AS passt
		while (!vergleicheKarte(so.getSpieler().getKarten().get(zahl), as.getKartenart(), so.getSpieler()))
		{
			System.out.println(
					"Sie k�nnen diese Karte nicht ablegen, bitte w�hlen Sie eine andere Karte oder heben und beenden 'h'");

			// Spieler gibt erneut eine Zahl ein
			String clientAntwort3 = br.readLine();
			fa.setText(clientAntwort3);

			// �berpr�fung ob es eine Zahl ist
			r = Parser.tryParseInt(clientAntwort3);
			zahl = r;

			// Kontrolliert ob die Zahl m�glich ist
			while ((zahl == null || zahl < 0 || zahl > so.getSpieler().getHandkartenSize())
					&& !clientAntwort3.equals("h"))
			{
				System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");

				// Spieler macht neue Eingabe
				String neueAntwort = br.readLine();
				fa.setText(neueAntwort);

				r = Parser.tryParseInt(neueAntwort);
				clientAntwort3 = neueAntwort;
				zahl = r;
			}

			// Spieler m�chte doch heben
			if (clientAntwort3.equals("h"))
			{
				so.getSpieler().aufnehmen(so.getKs());
				so.getSpieler().zeigeKarten();
				fa.setHeben(true);
				oos.writeObject(fa);
				oos.flush();
				System.out.println("Ihr Spielzug ist beendet");
				return;
			}
		}

		// wenn man eine Farbe ausw�hlen muss
		if (so.getSpieler().getKarten().get(zahl) instanceof Farbenkarte)
		{
			// Spieler wird gefragt welche Farbe er w�hlen m�chte
			System.out.println("W�hlen Sie eine Farbe:");
			System.out.println("'r' = rot; 'g' = gr�n; 'y' = gelb; 'b' = blau");

			test4 = br.readLine();

			while (!test4.equals("r") && !test4.equals("g") && !test4.equals("y") && !test4.equals("b"))
			{
				System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
				String test5 = br.readLine();
				test4 = test5;
			}

			// Spieler setzt die gew�hlte Farbe
			farbe = farbeWaehlen(test4);
			fa.setFarbe(farbe);
		}

		oos.writeObject(fa);
		oos.flush();

		// Spieler bekommt den neuen Ablagestapel und seine aktualisierten Karten
		so = (ServerObject) ois.readObject();
		as.addKarten(so.getAs());
		as.zeigeAblagestapel();
		so.getSpieler().zeigeKarten();

	}

	public static String AbfrageText(String x)
	{

		String ausgabe = "";
		// Abfragen welcher Text kommmt, am Zug break / Spielende return / Sessionende
		// in eine Methode mit return

		if (x.contains("Am Zug ist: " + spielername))
		{
			// Spielzug beginnt
		}

		if (x.contains("Das Spiel ist beendet! "))
		{
			// Spiel ist beendet ruf Spielende auf
			spielEnde = true;
		}

		if (x.contains("Die Session ist beendet"))
		{
			// Session vorbei
			sessionEnde = true;
		}

		return ausgabe;
	}
}
