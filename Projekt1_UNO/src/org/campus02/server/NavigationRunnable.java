package org.campus02.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import org.campus02.datenbank.DBAbfragen;

public class NavigationRunnable implements Runnable {

	private Socket client;
	private GameManager gm;
	private DBAbfragen db;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private PlayerServer ps;

	public NavigationRunnable(Socket client, GameManager gm, DBAbfragen db, ObjectInputStream ois,
			ObjectOutputStream oos, PlayerServer ps) {
		this.client = client;
		this.gm = gm;
		this.db = db;
		this.ois = ois;
		this.oos = oos;
		this.ps = ps;
	}

	@Override
	public void run() {

		navigation();
	}

	public synchronized void navigation() {
	
		String antwortVonClientFürNavi = "";

		while (!antwortVonClientFürNavi.equals("exit")) // ACHTUNG while() auch bei Client
		{

			
		
			String auswahlNavi = "\nWas möchten Sie tun? \nHistorie für einen Spieler: 1 \nHistorie für die aktuelle Session: 2 \nBeenden und weiter spielen: exit";
			try {
				writeObject(auswahlNavi);
				antwortVonClientFürNavi = (String) readObject();
				
				

			switch (antwortVonClientFürNavi) {

			case "1":
				String fürWelchenNamen = (String) readObject();
				ArrayList<HashMap<String, String>> ergebnisFürName = gm.getHistorieforPlayer(fürWelchenNamen); 
				writeObject(ergebnisFürName);
				break;
			case "2":
				
				ArrayList<HashMap<String, String>> ergebnisFürSession = gm.getHistorieforSession();
				writeObject(ergebnisFürSession);
				break;
			case "exit":
				if(!gm.getAllPointsFromDB())
				{
					writeObject("Die anderen Spieler befinden sich noch im Menü - die nächste Runde startet in Kürze...\n");
				}
				else
				{
					writeObject("Die Session ist beendet - Danke für's Mitspielen! Wenn du weiter spielen möchtest, logge dich erneut ein.");
				}
				
					return; 		

			default:
				// Client bearbeitet default						
				break;
			}
			} catch(IOException e) {
				System.out.println("Der Client hat die Verbindung zum Server getrennt.");
//				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		
		}
	}

	public Object readObject() throws IOException, ClassNotFoundException {
		return ois.readObject();
	}

	public void writeObject(Object o) throws IOException {
		oos.writeObject(o);
		oos.flush();
		oos.reset();
	}

}
