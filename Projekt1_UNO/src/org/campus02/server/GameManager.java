package org.campus02.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.campus02.datenbank.DBAbfragen;

import KartenTest.AblegeStapel;
import KartenTest.ActionKarte;
import KartenTest.Aussetzen;
import KartenTest.Bot;
import KartenTest.Farbe;
import KartenTest.Farbenkarte;
import KartenTest.Karten;
import KartenTest.Kartenstapel;
import KartenTest.Parser;
import KartenTest.Plus2;
import KartenTest.Plus4;
import KartenTest.Richtungswechsel;
import KartenTest.Spielablauf;
import KartenTest.Spieler;
import KartenTest.Zahlenkarte;

public class GameManager implements Serializable
{
	private static final long serialVersionUID = 10L;
	public Spielablauf sa;
	private Socket client;
	public AblegeStapel as;
	public Kartenstapel ks;
	public ArrayList<Spieler> mitspieler = new ArrayList<>();
	public Spieler aktuellerSpieler;
	protected ArrayList<String> namen = new ArrayList<>();
	private DBAbfragen db;
	public boolean uno;
	int spieleranzahl;
	protected ArrayList<PlayerServer> psList = new ArrayList<>();
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private PlayerServer ps;
	private boolean spieleruno;

	public GameManager(DBAbfragen db)
	{
		this.as = new AblegeStapel();
		this.ks = new Kartenstapel();
		this.sa = new Spielablauf(as, ks, mitspieler);
		this.db = db;

	}

	public void spielStart()
	{

		ks.bef�llen();
		ks.mischen();
		sa.austeilen();

		System.out.println("---------------");
		// Kontrolle ob jeder Spieler Karten erh�lt
		for (Spieler spieler : mitspieler)
		{
			System.out
					.println("Spieler: " + spieler.getName() + " - Kartenanzahl: " + spieler.getAktuelleKartenanzahl());
		}
		System.out.println("-----------------\n");

		// Jedem Spieler werden seine Handkarten und der Ablegestapel geschickt
		System.out.println("Spiel wird gestartet");
		sendeSOAllenSpieler("Spiel wird gestartet");

		while (!hasPlayerWon())
		{

			ks.zeigeStapel();
			as.zeigeAblagestapel();
			System.out.println();

			aktuellerSpieler = sa.naechsterSpieler();
			System.out.println("Am Zug ist: " + aktuellerSpieler.getName() + "\n" + KartenanzahlAllerSpieler());
			aktuellerSpieler.zeigeKarten();
			spieleruno = false;

			// Jedem Spieler wird gesagt, wer am Zug ist
			sendeFrageAntwortAllenSpieler(
					"Am Zug ist: " + aktuellerSpieler.getName() + "\n" + KartenanzahlAllerSpieler());

			// wenn der Spieler ein Bot ist, f�hre den Bot-Spielzug aus
			if (aktuellerSpieler instanceof Bot)
			{
				botSpielzug();

			} else
			{

				as.zeigeAblagestapel();

				// Counter und UNO wird zur�ckgesetzt
				uno = false;
				// counter = 10;
				// counterControll = true;

				// dem aktuellen Spieler werden seine Karten, der AS und der KS geschickt
				sendeSOAktuellemSpieler(aktuellerSpieler.getName() + ", Sie sind am Zug");

				// es wird kontrolliert ob Spieler mehr als eine Karte hat
				if (aktuellerSpieler.getAktuelleKartenanzahl() == 1)
				{
					uno = true;
				} else
				{
					// aktueller Spieler wird gefragt ob er UNO sagen m�chte
					System.out.println("M�chten Sie UNO sagen? Dr�cken Sie 'j' oder 'n'");
					sendeFrageAntwort("M�chten Sie UNO sagen? Dr�cken Sie 'j' oder 'n'");

					// Eingabe vom Client kommt zur�ck
					String todo = empfangeFrageAntwort().getText();
					String antwort = todo;

					// solange Client falsche Eingabe t�tigt
					while (!antwort.equals("j") && !antwort.equals("n"))
					{
						// sendet Spieler falsche Eingabe
						System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
						sendeFrageAntwort("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");

						// Spieler wiederholt die Eingabe
						String neueAntwort = empfangeFrageAntwort().getText();
						antwort = neueAntwort;

					}

					// wenn der Spieler UNO sagt
					if (antwort.equals("j"))
					{
						if (!spielerSagtUno())
						{
							continue;
						}
					}
				}

				if (!spieleruno)
				{
					// aktueller Spieler wird gefragt ob er legen oder heben m�chte
					System.out.println(
							"K�nnen Sie eine Karte ablegen oder m�chten Sie eine abheben? Dr�cken Sie 'l' oder 'h'");
					sendeFrageAntwort(
							"K�nnen Sie eine Karte ablegen oder m�chten Sie eine abheben? Dr�cken Sie 'l' oder 'h'");

					// Eingabe vom Client kommt zur�ck
					String todo = empfangeFrageAntwort().getText();
					String antwort = todo;

					// solange Client falsche Eingabe t�tigt
					while (!antwort.equals("l") && !antwort.equals("h"))
					{
						// sendet Spieler falsche Eingabe
						System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
						sendeFrageAntwort("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");

						// Spieler wiederholt die Eingabe
						String neueAntwort = empfangeFrageAntwort().getText();
						antwort = neueAntwort;

					}

					// wenn spieler legen m�chte
					if (antwort.equals("l"))
					{
						// Spieler wird geschickt, Karte ausw�hlen
						System.out.println("W�hlen Sie eine Karte aus Ihrem Deck: Zahl von 0 - "
								+ aktuellerSpieler.getHandkartenSize() + ", oder heben und beenden 'h'");
						sendeFrageAntwort("W�hlen Sie eine Karte aus Ihrem Deck: Zahl von 0 - "
								+ aktuellerSpieler.getHandkartenSize() + ", oder heben und beenden 'h'");

						// hier wird am Client �berpr�ft ob er die Karte legen darf

						// Server empf�ngt die gesendete Karte
						FrageAntwort fa = empfangeFrageAntwort();

						// wenn Spieler normal abgelegt hat
						if (!fa.getHeben())
						{
							String test = fa.getText();
							Farbe farbe = fa.getFarbe();
							// eingegebene Karte wird auf den Ablagestapel gelegt
							as = KarteAblegen(aktuellerSpieler.ablegen(test));
							zeigeAblagestapel();

							if (as.getKartenart() instanceof Farbenkarte)
							{
								// die gew�hlte Farbe wird gesetzt
								as.getKartenart().setFarbe(farbe);
							}
							spielerLegtKarte();

							// Spieler wird aktueller as und hk gesendet
							sendeSOAktuellemSpieler("");

						} else if (fa.getHeben())
						{
							aktuellerSpieler.aufnehmen(abheben());
							continue;

						}
					}

					// wenn Spieler heben m�chte
					else if (antwort.equals("h"))
					{
						// Spieler hebt Karte ab
						aktuellerSpieler.aufnehmen(abheben());
						zeigeAblagestapel();
						zeigeSpielerkarten();

						// Spieler werden die aktuellen Karten geschickt
						sendeSOAktuellemSpieler("");

						// aktueller Spieler wird gefragt ob er UNO sagen m�chte
						System.out.println("M�chten Sie UNO sagen? Dr�cken Sie 'j' oder 'n'");
						sendeFrageAntwort("M�chten Sie UNO sagen? Dr�cken Sie 'j' oder 'n'");

						// Antwort vom Client ob j oder n
						todo = empfangeFrageAntwort().getText();
						antwort = todo;

						// solange Client falsche Eingabe t�tigt
						while (!antwort.equals("j") && !antwort.equals("n"))
						{
							// sendet Spieler falsche Eingabe
							System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
							sendeFrageAntwort("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");

							// Spieler wiederholt die Eingabe
							String neueAntwort = empfangeFrageAntwort().getText();
							antwort = neueAntwort;

						}

						// wenn der Spieler UNO sagt
						if (antwort.equals("j"))
						{
							if (!spielerSagtUno())
							{
								continue;
							}
						}

						// aktueller Spieler wird gefragt ob er legen oder heben m�chte
						System.out
								.println("K�nnen Sie eine Karte ablegen oder beenden m�chte? Dr�cken Sie 'l' oder 'b'");
						sendeFrageAntwort(
								"K�nnen Sie eine Karte ablegen oder m�chten Sie Ihren Zug beenden? Dr�cken Sie 'l' oder 'b'");

						// Antwort vom Client wird kontrolliert - l oder h geschickt wurde
						String todo2 = empfangeFrageAntwort().getText();
						String antwort2 = todo2;

						// solange Client falsche Eingabe t�tigt
						while (!antwort2.equals("l") && !antwort2.equals("b"))
						{
							// Spieler bekommt Antwort, dass die Eingabe falsch war
							System.out.println("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");
							sendeFrageAntwort("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");

							String neueAntwort = empfangeFrageAntwort().getText();
							antwort2 = neueAntwort;

						}

						// wenn spieler legen m�chte
						if (antwort2.equals("l"))
						{
							// Spieler wird geschickt, Karte ausw�hlen
							System.out.println("W�hlen Sie eine Karte aus Ihrem Deck: Zahl von 0 - "
									+ aktuellerSpieler.getHandkartenSize() + ", oder heben und beenden 'h'");
							sendeFrageAntwort("W�hlen Sie eine Karte aus Ihrem Deck: Zahl von 0 - "
									+ aktuellerSpieler.getHandkartenSize() + ", oder heben und beenden 'h'");

							// hier wird am Client �berpr�ft ob er die Karte legen darf

							// Server empf�ngt die gesendete Karte
							FrageAntwort fa = empfangeFrageAntwort();

							// wenn Spieler normal abgelegt hat
							if (!fa.getHeben())
							{
								String test = fa.getText();
								Farbe farbe = fa.getFarbe();
								// eingegebene Karte wird auf den Ablagestapel gelegt
								as = KarteAblegen(aktuellerSpieler.ablegen(test));
								zeigeAblagestapel();

								if (as.getKartenart() instanceof Farbenkarte)
								{
									// die gew�hlte Farbe wird gesetzt
									as.getKartenart().setFarbe(farbe);
								}
								spielerLegtKarte();

								// Spieler wird aktueller as und hk gesendet
								sendeSOAktuellemSpieler("");
							} else if (fa.getHeben())
							{
								aktuellerSpieler.aufnehmen(abheben());
								continue; // h�pft ganz raus
							}
						}

						// wenn der Spieler nicht ablegen kann und seinen Spielzug beenden m�chte
						else if (antwort2.equals("b"))
						{
							// sendet Spieler - Spielzug ist beendet
							sendeFrageAntwort("Ihr Spielzug ist beendet \n");
							System.out.println("Ihr Spielzug ist beendet \n");
						}

					}
					if (aktuellerSpieler.getAktuelleKartenanzahl() == 1
							|| aktuellerSpieler.getAktuelleKartenanzahl() == 0)
					{
						if (!uno)
						{
							// wenn Spieler vergessen hat UNO zu sagen
							System.out.println("Sie haben vergessen 'UNO' zu sagen. Sie m�ssen 2 Karten heben \n");

							sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " hat vergessen 'UNO' zu sagen. "
									+ aktuellerSpieler.getName() + " muss 2 Karten heben \n");

							aktuellerSpieler.aufnehmen(abheben());
							aktuellerSpieler.aufnehmen(abheben());
						}
					}
				}
			} // Spielzug beendet

		} // while (!playerhaswon()) ? wenn ja -> spielEnde() und Navigation
		spielEnde();
		return;

	}

	public void sendeFrageAntwortAllenSpieler(String s)
	{
		for (PlayerServer ps : psList)
		{
			try
			{
				FrageAntwort fa = new FrageAntwort(s);
				ps.writeObject(fa);

			} catch (IOException e)
			{

				// wenn sich ein Spieler unerwartet abmeldet - funktioniert leider nicht
				

				// System.out.println("In der Exception, weil Socket weg ist");
				//
				// Spieler sp = new Spieler();
				//
				// for (PlayerServer p : psList)
				// {
				// System.out.println("Spieler in der PSList: " + p.getSpieler().getName());
				//
				// if (p.getSocket().equals(x))
				// {
				// System.out.println("Disconnected Player: " + p.getName());
				// sp.setName(p.getSpieler().getName());
				// psList.remove(p);
				// }
				// }
				//
				// Bot a = new Bot();
				// a.setKarten(sp.getKarten());
				// a.setName("Ersatzbot");
				// System.out.println("Spieler der weg ist: " + sp.getName());
				//
				// for (int i = 0; i < mitspieler.size(); i++)
				// {
				// if (sp.getName().equals(mitspieler.get(i).getName()))
				// {
				// int index = i;
				// System.out.println("Abgemeldeter Spieler: " + mitspieler.get(i).getName());
				// mitspieler.add(index, a);
				// mitspieler.remove(mitspieler.get(i));
				// }
				// }
				e.printStackTrace();
			}
		}
	}

	public void sendeFrageAntwortAllenAusserAkt(String s)
	{
		for (PlayerServer ps : psList)
		{
			if (ps.getName().equals(aktuellerSpieler.getName()))
			{
				continue;
			}
			try
			{
				FrageAntwort fa = new FrageAntwort(s);
				ps.writeObject(fa);

			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void sendeSOAllenSpieler(String s)
	{
		for (PlayerServer ps : psList)
		{
			ServerObject so = new ServerObject(ps.getSpieler(), as.getKartenart(), ks.obersteKarte(), s);

			try
			{
				ps.writeObject(so);

			} catch (IOException e)
			{
				e.printStackTrace();
			}

		}
	}

	public void sendeSOAktuellemSpieler(String s)
	{

		for (PlayerServer ps : psList)
		{
			ServerObject so = new ServerObject(ps.getSpieler(), as.getKartenart(), ks.obersteKarte(), s);

			if (aktuellerSpieler.getName().equals(ps.getName()))
			{
				try
				{
					ps.writeObject(so);
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

	}

	public void sendeFrageAntwort(String s)
	{

		for (PlayerServer ps : psList)
		{

			FrageAntwort fa = new FrageAntwort(s);
			if (aktuellerSpieler.getName().equals(ps.getName()))
			{

				try
				{
					ps.writeObject(fa);

				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	public ServerObject empfangeSOAktuellerSpieler()
	{

		ServerObject serverObject = new ServerObject();

		for (PlayerServer ps : psList)
		{
			if (aktuellerSpieler.getName().equals(ps.getName()))
			{

				try
				{
					serverObject = (ServerObject) ps.readObject();

				} catch (ClassNotFoundException | IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return serverObject;
	}

	public FrageAntwort empfangeFrageAntwort()
	{
		FrageAntwort fa = new FrageAntwort();

		for (PlayerServer ps : psList)
		{
			if (aktuellerSpieler.getName().equals(ps.getName()))
			{

				try
				{
					fa = (FrageAntwort) ps.readObject();

				} catch (ClassNotFoundException | IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return fa;
	}

	public Socket getClient()
	{
		return client;
	}

	public void setClient(Socket client)
	{
		this.client = client;
	}

	public void spielerLegtKarte()
	{

		if (as.getKartenart() instanceof Aussetzen)
		{
			System.out.println(mitspieler.get(0).getName() + " Sie m�ssen leider eine Runde aussetzen");
			sendeFrageAntwortAllenAusserAkt(mitspieler.get(0).getName() + " Sie m�ssen leider eine Runde aussetzen");

			naechsterSpieler();
			return;

		} else if (as.getKartenart() instanceof Richtungswechsel)
		{
			System.out.println(aktuellerSpieler.getName() + " hat einen Richtungswechsel gespielt");
			sendeFrageAntwortAllenAusserAkt(aktuellerSpieler.getName() + " hat einen Richtungswechsel gespielt");

			Collections.reverse(mitspieler);
			naechsterSpieler();
			return;

		}

		else if (as.getKartenart() instanceof Plus2)
		{
			System.out.println(mitspieler.get(0).getName() + "  Sie m�ssen 2 Karten abheben");
			sendeFrageAntwortAllenAusserAkt(mitspieler.get(0).getName() + "  Sie m�ssen 2 Karten abheben");

			mitspieler.get(0).aufnehmen(abheben());
			mitspieler.get(0).aufnehmen(abheben());

			naechsterSpieler();
			return;

		} else if (as.getKartenart() instanceof Plus4)
		{
			System.out.println(mitspieler.get(0).getName() + "  Sie m�ssen 4 Karten abheben");
			sendeFrageAntwortAllenAusserAkt(mitspieler.get(0).getName() + "  Sie m�ssen 4 Karten abheben");

			mitspieler.get(0).aufnehmen(abheben());
			mitspieler.get(0).aufnehmen(abheben());
			mitspieler.get(0).aufnehmen(abheben());
			mitspieler.get(0).aufnehmen(abheben());

			naechsterSpieler();
			return;
		}
	}

	public void botSpielzug()
	{
		Bot b = (Bot) aktuellerSpieler;
		zeigeAblagestapel();
		b.botZeigtKarten();

		boolean botAntwort = b.vergleiche(as.getKartenart());

		if (botAntwort)
		{
			as = KarteAblegen(b.botLegtKarte(as.getKartenart()));

			if (as.getKartenart() instanceof Aussetzen)
			{
				System.out.println(mitspieler.get(0).getName() + " - Sie m�ssen leider eine Runde aussetzen");

				sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " hat Aussetzen gespielt, "
						+ mitspieler.get(0).getName() + " - Sie m�ssen leider eine Runde aussetzen");

				naechsterSpieler();
			}
			if (as.getKartenart() instanceof Richtungswechsel)
			{
				System.out.println(aktuellerSpieler.getName() + " -  hat einen Richtungswechsel gespielt");
				sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " -  hat einen Richtungswechsel gespielt");

				Collections.reverse(mitspieler);
				naechsterSpieler();
			}
			if (as.getKartenart() instanceof Plus2)
			{
				System.out.println(mitspieler.get(0).getName() + " - Sie m�ssen 2 Karten abheben");

				mitspieler.get(0).aufnehmen(abheben());
				mitspieler.get(0).aufnehmen(abheben());

				sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " hat eine Plus 2 gespielt, "
						+ mitspieler.get(0).getName() + " muss 2 Karten abheben");

				naechsterSpieler();
			}
			if (as.getKartenart() instanceof Plus4)
			{
				System.out.println(mitspieler.get(0).getName() + " - Sie m�ssen 4 Karten abheben");

				mitspieler.get(0).aufnehmen(abheben());
				mitspieler.get(0).aufnehmen(abheben());
				mitspieler.get(0).aufnehmen(abheben());
				mitspieler.get(0).aufnehmen(abheben());

				sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " hat eine Plus 4 gespielt, "
						+ mitspieler.get(0).getName() + " muss 4 Karten abheben");

				naechsterSpieler();
			}

			b.botZeigtKarten();
			return;
		} else
		{
			b.botHebtKarte(abheben());
			b.botZeigtKarten();

			botAntwort = b.vergleiche(as.getKartenart());
			if (botAntwort)
			{
				as = KarteAblegen(b.botLegtKarte(as.getKartenart()));

				if (as.getKartenart() instanceof Aussetzen)
				{
					System.out.println(aktuellerSpieler.getName() + " hat Aussetzen gespielt, "
							+ mitspieler.get(0).getName() + " - Sie m�ssen leider eine Runde aussetzen");

					sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " hat Aussetzen gespielt, "
							+ mitspieler.get(0).getName() + " - Sie m�ssen leider eine Runde aussetzen");

					naechsterSpieler();
				}
				if (as.getKartenart() instanceof Richtungswechsel)
				{
					System.out.println(aktuellerSpieler.getName() + " hat einen Richtungswechsel gespielt");
					Collections.reverse(mitspieler);

					sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " hat einen Richtungswechsel gespielt");

					naechsterSpieler();
				}
				if (as.getKartenart() instanceof Plus2)
				{
					System.out.println(mitspieler.get(0).getName() + " Sie m�ssen 2 Karten abheben");

					mitspieler.get(0).aufnehmen(abheben());
					mitspieler.get(0).aufnehmen(abheben());

					sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " hat eine Plus 2 gespielt, "
							+ mitspieler.get(0).getName() + " muss 2 Karten abheben");

					naechsterSpieler();
				}
				if (as.getKartenart() instanceof Plus4)
				{
					System.out.println(mitspieler.get(0).getName() + " Sie m�ssen 4 Karten abheben");

					mitspieler.get(0).aufnehmen(abheben());
					mitspieler.get(0).aufnehmen(abheben());
					mitspieler.get(0).aufnehmen(abheben());
					mitspieler.get(0).aufnehmen(abheben());

					sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " hat eine Plus 4 gespielt, "
							+ mitspieler.get(0).getName() + " muss 4 Karten abheben");

					naechsterSpieler();
				}
				return;
			}
			b.botZeigtKarten();

			return;
		}
	}

	public boolean spielerSagtUno()
	{

		System.out.println(aktuellerSpieler.getName() + " sagt UNO \n");
		ks.zeigeStapel();
		sendeFrageAntwortAllenSpieler(aktuellerSpieler.getName() + " sagt UNO \n");

		// wenn der Spieler mehr als 2 Karten in der Hand hat
		if (aktuellerSpieler.getAktuelleKartenanzahl() > 2)
		{
			spieleruno = true;

			System.out.println(aktuellerSpieler.getName() + " hatte noch zu viele Karten in der Hand");
			System.out.println(aktuellerSpieler.getName() + " muss 2 Karten abheben");
			System.out.println();

			aktuellerSpieler.zeigeKarten();

			aktuellerSpieler.aufnehmen(abheben());
			aktuellerSpieler.aufnehmen(abheben());

			// Spieler wird aktualisierte Hand geschickt
			sendeSOAktuellemSpieler(
					"Sie k�nnen nicht UNO sagen, Sie haben " + aktuellerSpieler.getAktuelleKartenanzahl()
							+ " Karten in der Hand \nSie m�ssen 2 Strafkarten heben");

			aktuellerSpieler.zeigeKarten();
			naechsterSpieler();
			return false;
		}
		uno = true;

		return true;
	}

	public Karten abheben()
	{
		Karten temp = null;

		try
		{
			temp = ks.karteAbheben();
		} catch (IndexOutOfBoundsException e)
		{
			while (as.getSize() > 0)
			{
				ks.addKarten(as.remove());
			}

			ks.mischen();
			temp = ks.karteAbheben();

		}
		return temp;
	}

	public AblegeStapel KarteAblegen(Karten k)
	{

		if (vergleicheKarten(k) == false)
		{

			// Spieler wird geschickt, dass er die Karte nicht drauflegen darf
			System.out.println("Sie k�nnen diese Karte nicht ablegen! \n"
					+ "W�hlen Sie eine neue Karte aus Ihrem Deck: Zahl von 0 - "
					+ aktuellerSpieler.getHandkartenSize());
			sendeFrageAntwort("Sie k�nnen diese Karte nicht ablegen! \n"
					+ "W�hlen Sie eine neue Karte aus Ihrem Deck: Zahl von 0 - "
					+ aktuellerSpieler.getHandkartenSize());

			zeigeSpielerkarten();

			String test3 = empfangeFrageAntwort().getText();
			Integer x = Parser.tryParseInt(test3);

			// Server kontrolliert ob die Zahl m�glich ist
			while (x == null || x < 0 || x > aktuellerSpieler.getHandkartenSize())
			{
				sendeFrageAntwort("Falsche Eingabe! Bitte wiederholen Sie die Eingabe");

				String neueAntwort = empfangeFrageAntwort().getText();
				test3 = neueAntwort;
				x = Parser.tryParseInt(neueAntwort);
			}

			// wenn die Karte passt, wird sie abgelegt
			as = KarteAblegen(aktuellerSpieler.ablegen(test3));

		} else if (vergleicheKarten(k) == true)
		{
			as.addKarten(k);
		}
		return as;
	}

	public boolean vergleicheKarten(Karten k)
	{
		Karten stapelKarte = as.getKartenart();

		if (k instanceof Zahlenkarte || k instanceof ActionKarte)
		{
			if (stapelKarte.getFarbe().equals(k.getFarbe()) || stapelKarte.getWert().equals(k.getWert()))
			{
				return true;
			} else if (stapelKarte.getWert() == k.getWert())
			{
				return true;
			}

		} else if (k instanceof Farbenkarte)
		{
			return true;
		}

		aktuellerSpieler.aufnehmen(k);
		return false;
	}

	public boolean hasPlayerWon()
	{
		if (aktuellerSpieler == null)
			return false;

		return aktuellerSpieler.getAktuelleKartenanzahl() == 0;
	}

	public Spieler naechsterSpieler()
	{
		Spieler s = mitspieler.remove(0);
		mitspieler.add(s);
		return s;
	}

	public void sessionEnde()
	{
		// alle internen Listen werden bereinigt
		mitspieler.clear();
		namen.clear();
		psList.clear();
		db.clearSessionListe();
	}

	public void spielEnde()
	{
		// an alle Spieler:
		System.out.println("Das Spiel ist beendet");
		System.out.println();
		System.out.println(aktuellerSpieler.getName() + " hat gewonnen");
		System.out.println();

		sendeFrageAntwortAllenSpieler("Das Spiel ist beendet! " + "\n " + aktuellerSpieler.getName() + " hat gewonnen");

		for (int i = 0; i < mitspieler.size(); i++)
		{
			mitspieler.get(i).zeigeKarten();
		}

		ArrayList<Karten> angezeigteKarten = new ArrayList<>();

		for (Spieler sp : mitspieler)
		{
			int summe = 0;
			for (Karten k : sp.getKarten().getHk())
			{
				summe += k.getPunkte();
				angezeigteKarten.add(k);
			}

			String nameAusDatenbank = db.getNameAusDatenbank(sp.getName());

			if (nameAusDatenbank.equals(sp.getName()))
			{
				db.updateSession(summe, sp.getName());
			}
		}

		// Handkarten werden gel�scht
		for (Spieler sp : mitspieler)
		{
			sp.getKarten().getHk().clear();
		}

		// beide Stapel werden gel�scht
		as.clear();
		ks.clear();
		aktuellerSpieler = null; // aktuellen Spieler auf null setzen, da sonst hasPlayerWon in der n�chsten
									// Session auf true steht

	}

	public boolean getAllPointsFromDB()
	{

		boolean result = false;

		ArrayList<HashMap<String, String>> punkte = db.getAllPunkteForSession();

		for (HashMap<String, String> map : punkte)
		{
			String s = map.get("SessionScore");
			int p = Integer.parseInt(s);

			if (p >= 500)
			{

				result = true;
			}

		}
		return result;
	}

	public ArrayList<HashMap<String, String>> getHistorieforSession()
	{

		return db.getHistorieForSessionID();

	}

	public ArrayList<HashMap<String, String>> getHistorieforPlayer(String name)
	{

		ArrayList<HashMap<String, String>> namen = db.getalleNamenAusDatenbank();
		ArrayList<HashMap<String, String>> keinSpieler = new ArrayList<HashMap<String, String>>();

		ArrayList<String> spielerName = new ArrayList<>();

		for (HashMap<String, String> map : namen)
		{
			String nameAusDB = map.get("Nickname");

			spielerName.add(nameAusDB);
		}

		if (!spielerName.contains(name))
		{

			return keinSpieler;
		} else
		{

			return db.getHistorieForNickname(name);

		}
	}

	public void zeigeSpielerkarten()
	{
		aktuellerSpieler.zeigeKarten();
	}

	public void zeigeAblagestapel()
	{
		as.zeigeAblagestapel();
	}

	public String KartenanzahlAllerSpieler()
	{
		String sp = "";
		Integer anzahl = 0;
		String alles = "";

		for (Spieler spieler : mitspieler)
		{
			sp = spieler.getName();
			anzahl = spieler.getAktuelleKartenanzahl();
			alles += sp + " - akt. Kartenanzahl: " + anzahl.toString() + "\n";
		}

		return alles;
	}

	public ObjectInputStream getOis()
	{
		return ois;
	}

	public void setOis(ObjectInputStream ois)
	{
		this.ois = ois;
	}

	public ObjectOutputStream getOos()
	{
		return oos;
	}

	public void setOos(ObjectOutputStream oos)
	{
		this.oos = oos;
	}

	public PlayerServer getPs()
	{
		return ps;
	}

	public void setPs(PlayerServer ps)
	{
		this.ps = ps;
	}

}
