package org.campus02.server;

import java.io.Serializable;

import KartenTest.Handkarten;
import KartenTest.Karten;
import KartenTest.Spieler;

public class ServerObject implements Serializable
{
	
	
	private static final long serialVersionUID = 1L;
	private Spieler spieler;
	private Karten as;
	private Karten ks;
	private String todo = "";
	private Handkarten hk;
	
	
	public ServerObject(Spieler spieler, Karten as, Karten ks, String todo)
	{
		this.spieler = spieler;
		this.as = as;
		this.ks = ks;
		this.todo = todo;
	}
	
	public ServerObject(Spieler spieler, String todo)
	{
		this.spieler = spieler;
		this.todo = todo;
	}
	
	public ServerObject(Spieler spieler, Karten as, Karten ks, Handkarten hk, String todo)
	{
		this.spieler = spieler;
		this.as = as;
		this.ks = ks;
		this.hk = hk;
		this.todo = todo;
	}
	
	
	public ServerObject()
	{
		
	}
	
	

	public Spieler getSpieler()
	{
		return spieler;
	}


	public void setSpieler(Spieler spieler)
	{
		this.spieler = spieler;
	}


	public Karten getAs()
	{
		return as;
	}


	public void setAs(Karten as)
	{
		this.as = as;
	}


	public Karten getKs()
	{
		return ks;
	}


	public void setKs(Karten ks)
	{
		this.ks = ks;
	}


	public String getTodo()
	{
		return todo;
	}


	public void setTodo(String todo)
	{
		this.todo = todo;
	}

	public Handkarten getHk()
	{
		return hk;
	}

	public void setHk(Handkarten hk)
	{
		this.hk = hk;
	}
	
	
	
}
