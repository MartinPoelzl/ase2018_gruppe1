package org.campus02.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.campus02.datenbank.DBAbfragen;

import KartenTest.Spieler;

public class RegisterRunnable implements Runnable
{
	private Socket client;
	private GameManager gm;
	private DBAbfragen db;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private PlayerServer ps;
	

	public RegisterRunnable(Socket client, GameManager gm, DBAbfragen db, ObjectInputStream ois, ObjectOutputStream oos, PlayerServer ps)
	{
		super();
		this.client = client;
		this.gm = gm;
		this.db = db;
		this.ois = ois;
		this.oos = oos;
		this.ps = ps;
	}

	public RegisterRunnable(Socket client, GameManager gm, DBAbfragen db)
	{
		super();
		this.client = client;
		this.gm = gm;
		this.db = db;
	}
	
	@Override
	public void run()
	{

		try
		{
					
			String name = (String) readObject();
			System.out.println(name);

			while (checkNames(name))
			{
				String nameVergeben = "Dieser Name ist bereits vergeben, bitte geben Sie einen neuen Namen ein:";
				writeObject((nameVergeben));
				name = (String) readObject();
			}

			String welcome = "Login erfolgreich - das Spiel startet in K�rze...";
			writeObject(welcome);

			
			// Mitspieler werden zu Datenbank und mitspieler Liste und zur HashMap (Spielername, Socket) hinzugef�gt
			addToDatabase(name);
			
			
			

		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		} 

	}

	public synchronized boolean checkNames(String name)
	{
		if (gm.namen.contains(name))
		{
			return true;
		} else
		{
			gm.namen.add(name);
		}
		return false;
	}

	public synchronized void addToDatabase(String name)
	{
		Spieler s = new Spieler();
		s.setName(name);
		ps.setName(name);
		ps.setSpieler(s);
		gm.mitspieler.add(s);
		gm.psList.add(ps);

		
		db.logIntoSession(name);
		
		//gm.setSessionID();
	}

	public Object readObject() throws IOException, ClassNotFoundException
	{
		return ois.readObject();
	}

	public void writeObject(Object o) throws IOException
	{
		oos.writeObject(o);
		oos.flush();
		oos.reset();
	}

}
