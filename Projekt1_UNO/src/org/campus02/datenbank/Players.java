package org.campus02.datenbank;

public class Players
{
	
		private String nickname;
		private int sessionID;
		private int sessionScore;
		
		public String getNickname()
		{
			return nickname;
		}
		public void setNickname(String nickname)
		{
			this.nickname = nickname;
		}
		public int getSessionID()
		{
			return sessionID;
		}
		public void setSessionID(int sessionID)
		{
			this.sessionID = sessionID;
		}
		public int getPunkte()
		{
			return sessionScore;
		}
		public void setPunkte(int punkte)
		{
			this.sessionScore = punkte;
		}
		@Override
		public String toString()
		{
			return "HighScore [nickname=" + nickname + ", sessionID=" + sessionID + ", punkte=" + sessionScore + "]";
		}
			
	

}
