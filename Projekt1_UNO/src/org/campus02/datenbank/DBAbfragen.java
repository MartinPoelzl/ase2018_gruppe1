package org.campus02.datenbank;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class DBAbfragen

{
	private SqliteClient client;
	private ArrayList<String> players = new ArrayList<String>();
	private int count=0;

	private static final String CREATETABLE = "CREATE TABLE Players (Nickname varchar(100) NOT NULL, SessionID, SessionScore int, CONSTRAINT PK_Players PRIMARY KEY (Nickname, SessionID));";
	private static final String INSERT_INTO_PLAYERS = "INSERT INTO Players (Nickname, SessionID, SessionScore) VALUES ('%1s', %2d, %3d);";
	private static final String SELECT_BYPLAYER = "SELECT Nickname, SessionScore, SessionID FROM Players WHERE Nickname = '%1s';";
	private static final String SELECT_SUMBYPLAYER = "SELECT SUM(SessionScore) AS Gesamtpunktezahl FROM Players WHERE Nickname = '%1s';";
	private static final String SELECT_BYSESSION = "SELECT Nickname, SessionScore, SessionID FROM Players WHERE SessionID = %1d;";
	private static final String UPDATE_SESSION = "UPDATE Players SET SessionScore = SessionScore + %1d  WHERE Nickname = '%2s' AND SessionID= %3d;";
	
	private static final String SELECT_SESSIONID= "SELECT SessionID from Players ORDER BY SessionID DESC LIMIT 1;";
	private static final String SELECT_SESSIONNAME= "SELECT Nickname from Players WHERE Nickname= '%1s' AND SessionID= %2d;";
	private static final String SELECT_NAMES= "SELECT Nickname from Players;";
	private static final String SELECT_PUNKTE= "SELECT SessionScore from Players WHERE SessionID= %1d;";
	
	
	
	public DBAbfragen() {
		
		try
		{
			this.client = new SqliteClient("uno.sqlite");
			if (!client.tableExists("Players"))
			{
				client.executeStatement(CREATETABLE);
			}


		} catch (SQLException ex)
		{
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
	}

//	public void init()
//	{
//		try
//		{
//			client = new SqliteClient("uno.sqlite");
//			if (!client.tableExists("Players"))
//			{
//				client.executeStatement(CREATETABLE);
//			}
//
//			// wenn Session vorbei, dann erh�he Z�hler um 1. Ist der Fall, wenn kein Spieler
//			// mehr vorhanden ODER 500 Pkt erreicht wurden
//
//			// Pr�fen, on Name schon vorhanden ist
//
//			// Log-In in bestehende Session nicht m�glich. Erst wenn Session zuende ist.
//			// Dann wird Z�hler erh�ht.
//
//		} catch (SQLException ex)
//		{
//			System.out.println("Ups! Something went wrong:" + ex.getMessage());
//		}
//	}

	public void logIntoSession(String nickname)
	{

		try
		{
			
			// nickname kommt von Konsole

			// Wenn es schon einen Spieler gibt, soll die SessionID des vorigen Spielers
			// �bernommen werden, bis 4 Spieler vorhanden sind
			// Wenn es noch keinen Spieler gibt, SessionID wird erh�ht und zugewiesen

			if (count<4) {
				int zaehler = getSessionID();
			if (players.isEmpty())
			{
				zaehler++;
				players.add(nickname);
				client.executeStatement(String.format(INSERT_INTO_PLAYERS, nickname, zaehler, 0));
				count++;
			} else
			{
				client.executeStatement(String.format(INSERT_INTO_PLAYERS, nickname, zaehler, 0));
				count++;
			}
			}

		} catch (SQLException ex)
		{
			System.out.println("Nickname bereits vergeben, bitte w�hlen Sie einen anderen Namen " + ex.getMessage());
		}

	
	}
	
	public void clearSessionListe () {
		players.clear();
		count = 0;
	}

	public int getSessionID()
	{
		int tempZaehler =0;
		String tempString = "";
		ArrayList<HashMap<String, String>> result = null;
		HashMap<String, String> tempHash = null;
		
		try
		{
			result = client.executeQuery((String.format(SELECT_SESSIONID)));
			if(result.isEmpty())
			{
				return 0;
			}else
			{
			tempHash = result.get(0);
			tempString = tempHash.get("SessionID");
			tempZaehler = Integer.valueOf(tempString);
			}
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tempZaehler;		
	}
	
	public ArrayList<HashMap<String, String>> getalleNamenAusDatenbank() {
		
		ArrayList<HashMap<String, String>> resultNames = null;
				
		try
		{
						
			resultNames = client.executeQuery((String.format(SELECT_NAMES)));
			
					
		} catch (SQLException ex)
		{
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
		
		return resultNames;
	}
	
	public ArrayList<HashMap<String, String>> getAllPunkteForSession() {
		
		ArrayList<HashMap<String, String>> punkte = null;
				
		try
		{
			int tempZaehler = getSessionID();			
			punkte = client.executeQuery((String.format(SELECT_PUNKTE, tempZaehler)));
			
					
		} catch (SQLException ex)
		{
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
		
		return punkte;
	}
	
	public String getNameAusDatenbank(String name) {
		String nameausDatenbank = "";
		ArrayList<HashMap<String, String>> resultNames = null;
				
		try
		{
			
			 int tempZaehler = getSessionID();
						
			resultNames = client.executeQuery((String.format(SELECT_SESSIONNAME, name, tempZaehler)));
			for (HashMap<String, String> map : resultNames)
			{
				nameausDatenbank = map.get("Nickname"); 
			}
					
		} catch (SQLException ex)
		{
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
		
		return nameausDatenbank;
	}
	
	public void updateSession(int punkte, String name) 
	{
			
		try
		{
			
			int tempZaehler = getSessionID();
						
				client.executeStatement(String.format(UPDATE_SESSION, punkte, name, tempZaehler));

					
		} catch (SQLException ex)
		{
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
		
	}

	public ArrayList<HashMap<String, String>> getHistorieForNickname(String nickname)
	{
		ArrayList<HashMap<String, String>> result = null;
		ArrayList<HashMap<String, String>> sumScore = null;

		try
		{
			result = client.executeQuery(String.format(SELECT_BYPLAYER, nickname));
			sumScore = client.executeQuery(String.format(SELECT_SUMBYPLAYER, nickname));
			result.addAll(sumScore);
			
		} catch (SQLException ex)
		{
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
		return result;
	}

	public ArrayList<HashMap<String, String>> getHistorieForSessionID() //int sessionID
	{
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();

		try
		{
			int tempZaehler = getSessionID();
			
			result = client.executeQuery(String.format(SELECT_BYSESSION, tempZaehler)); // sessionID
				

		} catch (SQLException ex)
		{
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
		return result;
	}
	
}
